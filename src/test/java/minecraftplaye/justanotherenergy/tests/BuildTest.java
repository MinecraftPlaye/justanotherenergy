/**
 * 
 */
package minecraftplaye.justanotherenergy.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import minecraftplaye.justanotherenergy.common.lib.Constants;

/**
 * Test to ensure a build does not
 * contain major flaws in the versions.
 * Example: Mod defines Minecraft Version
 * 1.10.2 while Gradle defines the Minecraft
 * Version 1.11.2. This build won't work
 * in real environment and thus
 * this test will fail.
 * 
 * @author MinecraftPlaye
 */
@DisplayName("Build Test")
class BuildTest {
    
    @Test
    @DisplayName("Minecraft Version Test")
    void minecraftTest() {
        assertTrue("[@MINECRAFT_VERSION@]".equals(Constants.MC_VERSION), "The Minecraft version"
                + "defined in the mod is different than the one defined in Gradle!");
    }
    
    @Test
    @DisplayName("Forge Version Test")
    void forgeVersionTest() {
        String replaceVersion = "@FORGE_VERSION@";
        String version = "required-after:forge@[" + replaceVersion + ",);";
        assertTrue(version.equals(Constants.FORGE_VERSION), "The Forge version"
                + "defined in the mod is different than the one defined in Gradle!");
    }
}