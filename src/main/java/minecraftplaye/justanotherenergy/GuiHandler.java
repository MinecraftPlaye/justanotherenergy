/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy;

import javax.annotation.Nullable;

import minecraftplaye.justanotherenergy.client.gui.GuiSolarPanel;
import minecraftplaye.justanotherenergy.client.gui.lexicon.GuiLexicon;
import minecraftplaye.justanotherenergy.common.container.ContainerSolarPanel;
import minecraftplaye.justanotherenergy.common.tileentities.TileEntitySolarPanel;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler {
	
    public static final int GUI_ID_LEXICON = 0;
	public static final int GUI_ID_SOLAR_PANEL = 1;
	
	@Override
	@Nullable
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		switch (ID) {
		    case GUI_ID_LEXICON:
		        return null;
			case GUI_ID_SOLAR_PANEL:
				return new ContainerSolarPanel(player.inventory, (TileEntitySolarPanel) world.getTileEntity(new BlockPos(x, y, z)));
		}
		
		return null;
	}
	
    @Override
	@Nullable
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		switch (ID) {
		    case GUI_ID_LEXICON:
		        return new GuiLexicon();
			case GUI_ID_SOLAR_PANEL:
				return new GuiSolarPanel(player.inventory, (TileEntitySolarPanel) world.getTileEntity(new BlockPos(x, y, z)));
		}
		return null;
	}
}