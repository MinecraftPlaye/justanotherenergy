/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.api;

/**
 * Interface used for registering blocks and items
 * with ore dictionary.
 * This interface was created for purpose of automation.
 * 
 * @author MinecraftPlaye
 */
public interface IItemOreDict {
	
	/**
	 * Register block/item with ore dictionary.
	 */
	void initOreDict();
}