
/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

/**
 * API for JustAnotherEnergy Mod.
 * 
 * @author MinecraftPlaye
 */
@API(apiVersion = "1.0.1", owner = "justanotherenergy", provides = "JustAnotherEnergy|API")
package minecraftplaye.justanotherenergy.api;

import net.minecraftforge.fml.common.API;