/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.api.lexicon;

/**
 * This interface is used to provide all pages
 * data from the <code>GuiScreen</code> without
 * providing the complete <code>GuiScreen</code>.
 * 
 * @author MinecraftPlaye
 */
public interface IChapterGui {
    
    /**
     * Returns the left side of the GUI.
     * This is the point the GUI starts
     * to get drawn on the left side of
     * the screen.
     * 
     * @return left start point
     */
    public int getLeft();
    
    /**
     * Returns the top side of the GUI.
     * This is the point the GUI starts
     * to get drawn on the top side of the
     * screen.
     * 
     * @return top start point
     */
    public int getTop();
    
    /**
     * Returns the width of the GUI.
     * 
     * @return GUI width
     */
    public int getWidth();
    
    /**
     * Returns the height of the GUI.
     * 
     * @return GUI height
     */
    public int getHeight();
    
    /**
     * Returns the localized title of the chapter.
     * 
     * @return the title of the chapter
     */
    public String getTitle();
}