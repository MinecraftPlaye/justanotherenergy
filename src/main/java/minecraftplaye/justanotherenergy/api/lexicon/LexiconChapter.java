/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.api.lexicon;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class represents a chapter in the
 * lexicon. One chapter often contains
 * multiply pages, for example one
 * page for an description and one
 * page to display the crafting recipe.
 * 
 * @author MinecraftPlaye
 */
public class LexiconChapter {
    
    /** Holds all pages of this chapter. */
    public final ArrayList<LexiconPage> pages = new ArrayList<>();
    
    /** The title of this chapter. */
    private final String title;
    
    /**
     * Creates a new chapter for the lexicon
     * with the given title. This title
     * is normally unlocalized.
     * 
     * @param title the title of this chapter
     */
    public LexiconChapter(String title) {
        this.title = title;
    }
    
    /**
     * Adds all pages to the chapter and returns the chapter
     * afterwards.
     * 
     * @param pages the pages this chapter has
     * @return this chapter
     */
    public LexiconChapter addAllPages(LexiconPage... pages) {
        this.pages.addAll(Arrays.asList(pages));
        return this;
    }
    
    /**
     * Returns the title of this chapter.
     * The title returned is normally unlocalized.
     * 
     * @return the title
     */
    public String getTitle() {
        return this.title;
    }
}