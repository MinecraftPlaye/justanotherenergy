/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

/**
 * This package holds all interfaces used by
 * the lexicon.
 * 
 * @author MinecraftPlaye
 */
package minecraftplaye.justanotherenergy.api.lexicon;