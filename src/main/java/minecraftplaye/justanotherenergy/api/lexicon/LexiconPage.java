/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.api.lexicon;

import javax.annotation.Nullable;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * This class represents a pageNumber to be
 * displayed in the lexicon.
 * <p>It cannot be initialized as it is
 * used as a base for all pageNumbers, as each pageNumber
 * might handle the specific functionality
 * different. For example a pageNumber with an
 * recipe cannot draw it as a normal text
 * pageNumber would draw its content to the screen.
 * 
 * @author MinecraftPlaye
 */
public abstract class LexiconPage {
    
    /**
     * The page number of this page.
     */
    private int pageNumber;
    
    /**
     * The unlocalized content of this page.
     * <p>Depending on the implementation is
     * it required to set this field before
     * the language file is loaded in. However,
     * this is not required.
     */
    @Nullable
    public String unlocalizedContent;
    
    /**
     * Initializes a page with the page number.
     * 
     * @param pageNumber the page number
     */
    public LexiconPage(int pageNumber) {
        this.pageNumber = pageNumber;
    }
    
    /**
     * Initializes a page with the page number and
     * its unlocalized content. The content may be
     * translated later.
     * 
     * @param pageNumber the page number
     * @param unlocalizedContent the unlocalized page content
     */
    public LexiconPage(int pageNumber, @Nullable String unlocalizedContent) {
        this.pageNumber = pageNumber;
        this.unlocalizedContent = unlocalizedContent;
    }
    
    /**
     * This method is called when the GUI for this page
     * is initialized. This means it gets called when the
     * GUI is displayed and when the window resizes.
     * It is the same as <code>initGui</code> in GuiScreen.
     * 
     * @param gui GUI object to provide access to data from GuiScreen
     */
    @SideOnly(Side.CLIENT)
    public abstract void initGui(IChapterGui gui);
    
    /**
     * Draws the page and all its components to the screen.
     * 
     * @param gui GUI object to provide access to data from GuiScreen
     * @param mouseX the position of the mouse on the x axis
     * @param mouseY the position of the mouse on the y axis
     */
    @SideOnly(Side.CLIENT)
    public abstract void renderScreen(IChapterGui gui, int mouseX, int mouseY);
    
    /**
     * Returns the page number of this page.
     * 
     * @return the page number
     */
    public int getPage() {
        return this.pageNumber;
    }
    
    /**
     * Returns the unlocalized content of this page.
     * 
     * @return the unlocalized page content
     */
    @Nullable
    public String getUnlocalizedContent() {
        return this.unlocalizedContent;
    }
}