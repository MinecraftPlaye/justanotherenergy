/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.api.lexicon;

import net.minecraft.item.ItemStack;

/**
 * 
 * @author MinecraftPlaye
 */
public interface IKnowledge {
    
    public boolean isKnowledgeUnlocked(KnowledgeType type, ItemStack stack);
    
    public void unlockKnowledge(KnowledgeType type, ItemStack stack);
}