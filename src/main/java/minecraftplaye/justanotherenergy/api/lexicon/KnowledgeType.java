/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.api.lexicon;

public class KnowledgeType {
    
    private int id;
    private int name;
    private int description;
    
    public int getID() {
        return this.id;
    }
}