/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.api;

import java.util.Comparator;
import java.util.stream.Stream;

import net.minecraft.util.IStringSerializable;

/**
 * Stores all chunk loader types.
 * 
 * @author MinecraftPlaye
 */
public enum ChunkLoaderTypes implements IStringSerializable {
    
    LOAD_01(0),
    LOAD_09(1),
    LOAD_25(2);
    
    private final int ID;
    
    private static final ChunkLoaderTypes[] META_LIST = Stream.of(values()).sorted(Comparator.comparing(ChunkLoaderTypes::getID)).toArray(ChunkLoaderTypes[]::new);
    
    private ChunkLoaderTypes(final int ID) {
        this.ID = ID;
    }
    
    @Override
    public String getName() {
        return String.valueOf(this.getID());
    }
    
    /**
     * @return The id of the chunk loader type
     */
    public int getID() {
       return this.ID; 
    }
    
    /**
     * Gets the chunk loader type from the given ID.
     * 
     * @param ID The ID to get the chunk loader type from.
     * @return The chunk loader type which matches the given ID.
     */
    public static ChunkLoaderTypes byMetadata(int ID) {
        if(ID < 0 || ID >= META_LIST.length)
            ID = 0;
        
        return META_LIST[ID];
    }
}