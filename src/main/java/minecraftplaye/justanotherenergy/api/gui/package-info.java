/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

/**
 * Holds the implementation for the custom GUI system.
 * 
 * @author MinecraftPlaye
 */
package minecraftplaye.justanotherenergy.api.gui;