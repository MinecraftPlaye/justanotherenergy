/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.api.gui;

import minecraftplaye.justanotherenergy.api.pollution.IChunkPollution;

import net.minecraft.world.World;

import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Renders {@link IEnergyStorage} and {@link IChunkPollution} 
 * related data to the screen. The methods provided
 * will be called automatically from the
 * {@link net.minecraftforge.client.event.RenderGameOverlayEvent.Post RenderGameOverlayEvent.Post} event
 * for all blocks the player is currently directly
 * looking at.
 * 
 * @author MinecraftPlaye
 */
public interface IRenderOverlay {
    
    /**
     * Used to render {@link IEnergyStorage} related data to the screen.
     * 
     * @param worldIn the world
     */
    @SideOnly(Side.CLIENT)
    void renderEnergy(World worldIn);
    
    /**
     * Used to render {@link IChunkPollution} related data to the screen.
     * 
     * @param worldIn the world
     */
    @SideOnly(Side.CLIENT)
    void renderPollution(World worldIn);
}