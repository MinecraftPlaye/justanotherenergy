/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.api.gui;

/**
 * This interface is used to determine if a GUI object can be invisible
 * or not.
 * 
 * @author MinecraftPlaye
 */
public interface IHideable {
	
	/**
	 * Sets if the GUI object is visible or invisible.
	 * 
	 * @param visible visibility of the GUI object
	 */
	void setIsVisible(boolean visible);
	
	/**
	 * Returns if the GUI object is visible.
	 * 
	 * @return whether the GUI is visible
	 */
	boolean isVisible();
}