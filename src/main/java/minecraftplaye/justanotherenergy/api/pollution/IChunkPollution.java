/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.api.pollution;

import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;

/**
 * Stores the pollution value for a specific chunk.
 * This value is stored for each chunk separately.
 * <p>If there is no {@link IChunkPollution} stored
 * for a chunk, the pollution value should be 0.
 * 
 * @author MinecraftPlaye
 */
public interface IChunkPollution {
    
    /**
     * Adds pollution to the chunk.
     * 
     * @param pollution The pollution to be added to the chunk.
     */
    void polluteChunk(double pollution);
    
    /**
     * Removes pollution from the chunk.
     * The chunk pollution in the chunk cannot be lower as 0.
     * 
     * @param pollution The pollution to be removed from the chunk.
     */
    void decreasePollution(double pollution);
    
    /**
     * <b>!! INTERNAL USE ONLY !!</b>
     * <p>Sets the pollution for the chunk.
     * <p>This method is required as for example the
     * network message handler has to be able to set the pollution
     * amount of a chunk in order for the client to receive the
     * correct values from the Server.
     * 
     * @param pollution The pollution to be set for the chunk.
     */
    void setPollution(double pollution);
    
    /**
     * Returns the amount of pollution in the chunk.
     * 
     * @return the amount of pollution
     */
    double getPollution();
    
    /**
     * Get the {@link World} containing the chunk.
     * 
     * @return the world
     */
    World getWorld();
    
    /**
     * Get the {@link ChunkPos} of the chunk.
     * 
     * @return the chunk position
     */
    ChunkPos getChunkPos();
}