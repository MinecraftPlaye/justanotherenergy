/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.api.pollution;

import javax.annotation.Nullable;

import net.minecraft.util.math.ChunkPos;

/**
 * A capability to hold the per-chunk {@link IChunkPollution} instances.
 * 
 * @author MinecraftPlaye
 */
public interface IChunkPollutionHolder {
    
    /**
     * Get the {@link IChunkPollution} for the specificied chunk.
     * 
     * @param pos The position of the chunk.
     * @return The {@link IChunkPollution}, or null if the chunk isn't loaded.
     */
    @Nullable
    IChunkPollution getChunkPollution(final ChunkPos pos);
    
    /**
     * Set the {@link IChunkPollution} for the specified chunk.
     * 
     * @param pos the chunk position
     * @param pollution the IChunkPollution
     */
    void setChunkPollution(final ChunkPos pos, final IChunkPollution pollution);
    
    /**
     * Remove the {@link IChunkPollution} for the specified chunk.
     * 
     * @param pos the chunk position
     */
    void removeChunkPollution(final ChunkPos pos);
}