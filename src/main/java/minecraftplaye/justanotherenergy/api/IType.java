/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.api;

import net.minecraft.util.IStringSerializable;

/**
 * An interface for blocks and items which have a 'custom' PropertyEnum.
 * Can be used as a helper for automatic block and item rendering registry.
 * 
 * @author MinecraftPlaye
 */
public interface IType extends IStringSerializable {
	
	/**
	 * Get the meta-data value of this blockstate.
	 * 
	 * @return The meta-data value
	 */
	public int getID();
}