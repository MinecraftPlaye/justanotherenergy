/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.api;

import java.util.ArrayList;
import java.util.List;

import minecraftplaye.justanotherenergy.api.lexicon.LexiconChapter;

public class JustAnotherEnergyAPI {
    
    public static final List<LexiconChapter> lexiconChapters = new ArrayList<>();
    
    /**
     * Registers a Lexicon Chapter.
     * 
     * @param chapter the chapter to be registered
     */
    public static void addChapter(LexiconChapter chapter) {
        JustAnotherEnergyAPI.lexiconChapters.add(chapter);
    }
}