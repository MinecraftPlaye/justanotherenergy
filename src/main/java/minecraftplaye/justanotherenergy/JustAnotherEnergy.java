/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy;

import minecraftplaye.justanotherenergy.common.lexicon.LexiconData;
import minecraftplaye.justanotherenergy.common.lib.ChunkManagerCallback;
import minecraftplaye.justanotherenergy.common.lib.Constants;
import minecraftplaye.justanotherenergy.common.lib.ModBlocks;
import minecraftplaye.justanotherenergy.common.lib.ModItems;
import minecraftplaye.justanotherenergy.common.lib.ModRecipes;
import minecraftplaye.justanotherenergy.common.network.NetworkHandler;
import minecraftplaye.justanotherenergy.common.power.pollution.CapabilityPollution;
import minecraftplaye.justanotherenergy.common.worldgen.ModWorldGen;

import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLFingerprintViolationEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLMissingMappingsEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(modid = Constants.MOD_ID, 
    name = Constants.MOD_NAME,
	version = Constants.VERSION + "-" + Constants.BUILD,
	dependencies = Constants.FORGE_VERSION + Constants.DEPENDENCIES,
	acceptedMinecraftVersions = Constants.MC_VERSION,
	useMetadata = false,
	canBeDeactivated = false,
	certificateFingerprint = Constants.FINGERPRINT) 
	//updateJSON = Constants.UPDATE)
public class JustAnotherEnergy {
	
    @Nonnull
	public static Logger LOGGER = LogManager.getLogger(Constants.MOD_ID);
	
    @Nullable
	@Mod.Instance(Constants.MOD_ID)
	public static JustAnotherEnergy instance;
	
    @Nullable
	@SidedProxy(clientSide = Constants.CLIENT_PROXY_LOCATION, serverSide = Constants.SERVER_PROXY_LOCATION)
	public static IProxy proxy;
	
	@Nonnull
	public static GuiHandler handler = new GuiHandler();
	
	public static boolean successSign = true;
	
	@Mod.EventHandler
	public void signing(FMLFingerprintViolationEvent event) {
	    if(event.isDirectory()) return; // check for development environment
	    
	    JustAnotherEnergy.successSign = false;
	    if(JustAnotherEnergy.getLogger() != null) {
	        JustAnotherEnergy.getLogger().error("The signature of the mod is invalid. {}", event.getSource().getAbsolutePath());
	        JustAnotherEnergy.getLogger().error("The signature of the mod is invalid. Observed {} fingerprints.", event.getFingerprints().size());
	        JustAnotherEnergy.getLogger().error("The signature of the mod is invalid. This may be caused due to a corrupted jar file.");
	        JustAnotherEnergy.getLogger().error("The signature of the mod is invalid. This version will NOT receive support.");
	        JustAnotherEnergy.getLogger().error("The signature of the mod is invalid. Proceed at your own risk.");
	    }
	}
	
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		// net handling mod config init items & blocks
		if(JustAnotherEnergy.getLogger() == null)
			JustAnotherEnergy.LOGGER = event.getModLog();
		if(JustAnotherEnergy.getInstance() == null) 
			JustAnotherEnergy.instance = this;
		
		ModBlocks.init();
		ModItems.init();
	    CapabilityPollution.register();
	    
		GameRegistry.registerWorldGenerator(new ModWorldGen(), 2);
		ForgeChunkManager.setForcedChunkLoadingCallback(JustAnotherEnergy.getInstance(), new ChunkManagerCallback());
	}
	
    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
    	// register gui, tile entites , crafting recipies (general event
        // handlers)
    	
        JustAnotherEnergy.proxy.registerEventHooks();
        
        ModRecipes.init();
    	NetworkHandler.init();
    	LexiconData.init();
    }
    
    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        // wrap things up .. runs after other mods do there init steps
    }
    
    @Mod.EventHandler
    public void missingMapping(FMLMissingMappingsEvent event) {
    	int remaps = 0;
    	/*FMLMissingMappingsEvent.MissingMapping newMapping = null;
    	
    	for(FMLMissingMappingsEvent.MissingMapping mapping : event.getAll()) {
    		++remaps;
    		MinecraftPower.getLogger().warn("The mapping for " + mapping + " was changed to " + newMapping + ".");
    	}*/
    	
    	if(remaps > 0)
    		JustAnotherEnergy.getLogger().info("Successfully remapped {} blocks/items/entities etc.", remaps);
    }
    
    @Nonnull
    public static Logger getLogger() {
    	return JustAnotherEnergy.LOGGER;
    }
    
    @Nullable
    public static JustAnotherEnergy getInstance() {
    	return JustAnotherEnergy.instance;
    }
}