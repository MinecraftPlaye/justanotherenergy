/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy;

public interface IProxy {
	
    public void registerEventHooks();
    
	/**
	 * Translates the given String and formats it.
	 * 
	 * @param unlocalized String to be translated
	 * @param args arguments to format the String
	 * @return localized and formated String
	 */
	public String localize(String unlocalized, Object... args);
}