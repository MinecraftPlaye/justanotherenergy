/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.client.gui;

import minecraftplaye.justanotherenergy.api.gui.IRenderOverlay;
import minecraftplaye.justanotherenergy.client.renderer.RenderHelper;
import minecraftplaye.justanotherenergy.common.lib.Constants;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.profiler.Profiler;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class UIEventHandler {
    
    private static final ResourceLocation UI_PROGRESS_TEXTURE = new ResourceLocation(Constants.MOD_ID.toLowerCase() + ":" + "textures/gui/container/solar_panel.png");
    
    @SubscribeEvent
    public void onDrawScreenPost(RenderGameOverlayEvent.Post event) {
        if(event.getType() == ElementType.ALL) {
            final Minecraft mc = Minecraft.getMinecraft();
            final RayTraceResult rayTrace = mc.objectMouseOver;
            
            if(rayTrace != null && rayTrace.typeOfHit == RayTraceResult.Type.BLOCK) {
                TileEntity tile = mc.world.getTileEntity(rayTrace.getBlockPos());
                if(tile instanceof IRenderOverlay) {
                    final Profiler profiler = mc.mcProfiler;
                    profiler.startSection(Constants.MOD_ID + "_renderOverlay_Energy");
                    ((IRenderOverlay)tile).renderEnergy(mc.world);
                    profiler.endSection();
                    profiler.startSection(Constants.MOD_ID + "_renderOverlay_Pollution");
                    ((IRenderOverlay)tile).renderPollution(mc.world);
                    profiler.endSection();
                }
            }
        }
    }
    
    public static void renderEnergyHUD(World worldIn, int energy, int maxEnergy, int color) {
        GlStateManager.disableDepth();
        RenderHelper.bindTexture(UI_PROGRESS_TEXTURE);
        
        ScaledResolution sRes = new ScaledResolution(Minecraft.getMinecraft());
        int guiStartX = (sRes.getScaledWidth() - 16) / 2;
        int guiStartY = (sRes.getScaledHeight() - 51) / 2;
        
        RenderHelper.renderTexturedRect(guiStartX, guiStartY, 0, 16, 17, 16, 51);
        int progressBar = UIEventHandler.getCapacityLeftScaled(51, energy, maxEnergy);
        
        RenderHelper.renderTexturedRect(guiStartX, guiStartY + 51 - progressBar, 0, 176, 51 - progressBar, 16, progressBar + 1);
        
        GlStateManager.enableDepth();
    }
    
    public static void renderPollutionHUD(World worldIn, int pollution) {
        GlStateManager.disableDepth();
        
        GlStateManager.enableDepth();
    }
    
    private static int getCapacityLeftScaled(int pixels, int stored, int capacity) {
        return stored != 0 && capacity != 0 ? stored * pixels / capacity : 0;
    }
}