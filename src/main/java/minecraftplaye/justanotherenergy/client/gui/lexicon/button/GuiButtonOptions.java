/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.client.gui.lexicon.button;

import java.awt.Rectangle;

import minecraftplaye.justanotherenergy.client.gui.base.widget.GuiTooltip;
import minecraftplaye.justanotherenergy.client.gui.lexicon.GuiLexicon;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiButtonOptions extends GuiButtonLexicon {
    
    private GuiTooltip buttonTooltip;
    
    public GuiButtonOptions(int buttonId, int x, int y, int widthIn, int heightIn) {
        super(buttonId, x, y, widthIn, heightIn, "");
        this.buttonTooltip = new GuiTooltip(new Rectangle(x, y, widthIn, heightIn), I18n.format("Test")); // TODO: broken code
        this.addToolTip(this.buttonTooltip);
    }
    
    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
        if(this.enabled && this.visible) {
            GlStateManager.color(1.f, 1.f, 1.f, 1.f);
            mc.getTextureManager().bindTexture(GuiLexicon.GUI_BACKGROUND);
            this.drawTexturedModalRect(this.x, this.y, 0, 183, 16, 16);
            
            this.hovered = this.isHoveredOver(mouseX, mouseY);
            if(this.hovered && this.getHoverState(this.hovered) == 2)
                this.drawTexturedModalRect(this.x, this.y, 16, 183, 16, 16);
        }
        super.drawButton(mc, mouseX, mouseY);
    }
}