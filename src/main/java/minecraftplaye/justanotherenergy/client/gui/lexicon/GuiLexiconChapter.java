/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.client.gui.lexicon;

import java.io.IOException;

import minecraftplaye.justanotherenergy.api.lexicon.IChapterGui;
import minecraftplaye.justanotherenergy.api.lexicon.LexiconChapter;
import minecraftplaye.justanotherenergy.api.lexicon.LexiconPage;
import minecraftplaye.justanotherenergy.client.gui.lexicon.button.GuiButtonBack;
import minecraftplaye.justanotherenergy.client.gui.lexicon.button.GuiButtonForward;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.resources.I18n;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiLexiconChapter extends GuiLexicon implements IChapterGui {
    
    private String title;
    private int currentPage = 0;
    private int maxPages = 0;
    private LexiconChapter chapter;
    
    public GuiLexiconChapter(LexiconChapter chapter) {
        this.chapter = chapter;
        this.title = I18n.format(this.chapter.getTitle());
    }
    
    @Override
    public void initSubGui() {
        this.maxPages = this.chapter.pages.size() - 1;
        
        for(GuiButton button : this.buttonList) {
            if(button instanceof GuiButtonForward)
                this.forward = (GuiButtonForward) button;
        }
        
        LexiconPage page = this.chapter.pages.get(this.currentPage);
        page.initGui(this);
    }
    
    @Override
    public void updateScreen() {
        if(this.currentPage == this.maxPages && this.forward.enabled == true)
            this.forward.enabled = false;
        else if(this.currentPage != this.maxPages && this.forward.enabled == false)
            this.forward.enabled = true;
    }
    
    @Override
    protected void drawUI(int mouseX, int mouseY, float partialTicks) {
        super.drawUI(mouseX, mouseY, partialTicks);
        
        LexiconPage page = this.chapter.pages.get(this.currentPage);
        page.renderScreen(this, mouseX, mouseY);
    }
    
    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        
        if(button instanceof GuiButtonForward) {
            if(this.maxPages + 1 > this.currentPage)
                this.currentPage++;
        }
        if(button instanceof GuiButtonBack) {
            if(this.currentPage == 0)
                this.mc.displayGuiScreen(new GuiLexicon());
            else
                this.currentPage--;
        }
    }
    
    @Override
    public int getLeft() {
        return this.left;
    }
    
    @Override
    public int getTop() {
        return this.top;
    }
    
    @Override
    public int getWidth() {
        return this.guiWidth;
    }
    
    @Override
    public int getHeight() {
        return this.guiHeight;
    }
    
    @Override
    public String getTitle() {
        return this.title;
    }
}
