/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.client.gui.lexicon;

import java.io.IOException;
import java.util.ArrayList;

import org.lwjgl.input.Mouse;

import minecraftplaye.justanotherenergy.api.JustAnotherEnergyAPI;
import minecraftplaye.justanotherenergy.api.lexicon.LexiconChapter;
import minecraftplaye.justanotherenergy.client.gui.lexicon.button.GuiButtonAchievement;
import minecraftplaye.justanotherenergy.client.gui.lexicon.button.GuiButtonBack;
import minecraftplaye.justanotherenergy.client.gui.lexicon.button.GuiButtonChapter;
import minecraftplaye.justanotherenergy.client.gui.lexicon.button.GuiButtonForward;
import minecraftplaye.justanotherenergy.client.gui.lexicon.button.GuiButtonOptions;
import minecraftplaye.justanotherenergy.common.lib.Constants;
import minecraftplaye.justanotherenergy.common.network.NetworkHandler;
import minecraftplaye.justanotherenergy.common.network.msg.MessageItemNameChanged;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiLexicon extends GuiScreen {
    
    /** Easter Eggs: */
    private int konamiIndex = 0;
    private static final String KONAMI_CODE_NAME = "コナミコマンド";
    private static final int[] KONAMI_CODE = { 200, 200, 208, 208, 203, 205, 203, 205, 48, 30 };
    
    private static final String GUI_LOCATION = "textures/gui/lexicon/";
    public static final ResourceLocation GUI_BACKGROUND = new ResourceLocation(Constants.MOD_ID + ":" + GuiLexicon.GUI_LOCATION + "background.png");
    
    protected int left;
    protected int top;
    private int guiScale = 3;
    protected final int guiWidth = 150;
    protected final int guiHeight = 182;
    
    private int page = 0;
    private int maxPages = 0;
    protected GuiButtonForward forward;
    protected GuiButtonBack back;
    
    private static final int CHAPTERS_PAGE = 10;
    private ArrayList<GuiButtonChapter> cbutton = new ArrayList<>();
    private ArrayList<LexiconChapter> chapters = new ArrayList<>(JustAnotherEnergyAPI.lexiconChapters);
    
    @Override
    public void initGui() {
        super.initGui();
        
        /* ------- Start of applying Scale ------- */
        
        ScaledResolution res = new ScaledResolution(this.mc);
        int tmpScale = this.mc.gameSettings.guiScale;
        int scale = Math.min(this.guiScale,  GuiLexicon.getMaxScale());
        
        if(scale > 0 && scale != tmpScale) {
            this.mc.gameSettings.guiScale = scale;
            res = new ScaledResolution(mc);
            this.width = res.getScaledWidth();
            this.height = res.getScaledHeight();
            this.mc.gameSettings.guiScale = tmpScale;
        }
        
        /* -------- End of applying Scale -------- */
        
        this.left = (this.width - this.guiWidth) / 2;
        this.top = (this.height - this.guiHeight) / 2;
        
        this.maxPages = (int) Math.ceil(this.chapters.size() / 10.f);
        this.buttonList.clear();
        
        GuiButtonOptions options = new GuiButtonOptions(0, this.left - 3, this.top + 10, 16, 16);
        GuiButtonAchievement achievements = new GuiButtonAchievement(1, this.left, this.top + 30, 9, 16);
        
        if(!this.isMainPage(this) || maxPages > 0) {
            int posY = this.top + this.guiHeight - 15;
            this.forward = new GuiButtonForward(2, this.left + this.guiWidth - 30, posY, 19, 9);
            this.back = new GuiButtonBack(3, this.left + 15, posY, 19, 9);
            
            this.buttonList.add(this.forward);
            this.buttonList.add(this.back);
        }
        
        this.buttonList.add(options);
        this.buttonList.add(achievements);
        
        this.initSubGui();
    }
    
    /**
     * Initializes all GUI elements for the GUI which inherits from this.
     * Called when the GUI is displayed and when the window resizes.
     */
    protected void initSubGui() {
        if(!this.chapters.isEmpty() && this.cbutton.isEmpty()) {
            int buttons = this.buttonList.size() - 1;
            int y = (this.top + 20) / GuiLexicon.CHAPTERS_PAGE + 10;
            for(int i = 0; i < GuiLexicon.CHAPTERS_PAGE; i++) {
                GuiButtonChapter button = new GuiButtonChapter(i + buttons, this.left + 20, y, 100, 10, null);
                this.cbutton.add(button);
                this.buttonList.add(button);
            }
        }
        
        if(!this.chapters.isEmpty()) {
            int buttons = this.buttonList.size() - 1;
            int y = (this.top + 20) / this.chapters.size() + 10;
            for(int i = 0; i < this.chapters.size(); i++) {
                y += 10;
                GuiButtonChapter button = new GuiButtonChapter(i + buttons, this.left + 20, y, 100, 10, this.chapters.get(i));
                button.displayString = I18n.format(this.chapters.get(i).getTitle());
                this.buttonList.add(button);
            }
        }
    }
    
    @Override
    public void updateScreen() {
        super.updateScreen();
        
        if(this.forward != null && this.back != null) {
            if(this.page == this.maxPages && this.forward.enabled == true)
                this.forward.enabled = false;
            else if(this.page != this.maxPages && this.forward.enabled == false)
                this.forward.enabled = true;
            
            if(this.page == 0 && this.back.enabled == true)
                this.back.enabled = false;
            else if(this.page != 0 && this.back.enabled == false)
                this.back.enabled = true;
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        GlStateManager.pushMatrix();
        
        /* ------- Start of applying Scale ------- */
        
        ScaledResolution res = new ScaledResolution(this.mc);
        int tmpScale = this.mc.gameSettings.guiScale;
        int scale = Math.min(this.guiScale,  GuiLexicon.getMaxScale());
        
        if(scale > 0 && scale != tmpScale) {
            this.mc.gameSettings.guiScale = scale;
            float calcScale = (float) scale / (float) res.getScaleFactor();
            GlStateManager.scale(calcScale, calcScale, calcScale);
            // start calculating new mouse positions
            res = new ScaledResolution(this.mc);
            mouseX = Mouse.getX() * res.getScaledWidth() / this.mc.displayWidth;
            mouseY = res.getScaledHeight() - Mouse.getY() * res.getScaledHeight() / this.mc.displayHeight - 1;
        }
        
        /* -------- End of applying Scale -------- */
        
        this.drawUI(mouseX, mouseY, partialTicks);
        // draw buttons while scale is still applied
        super.drawScreen(mouseX, mouseY, partialTicks);
        
        if(this.isMainPage(this))
            this.mc.fontRenderer.drawString(TextFormatting.BOLD + "Tech Lexicon", this.left + 20, this.top + 15, 2263842);
        
        // reset scale back to default
        this.mc.gameSettings.guiScale = tmpScale;
        GlStateManager.popMatrix();
    }
    
    protected void drawUI(int mouseX, int mouseY, float partialTicks) {
        GlStateManager.color(1.f, 1.f, 1.f, 1.f);
        this.mc.getTextureManager().bindTexture(GuiLexicon.GUI_BACKGROUND);
        this.drawTexturedModalRect(this.left, this.top, 0, 0, this.guiWidth, this.guiHeight);
    }
    
    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        
        if(button instanceof GuiButtonChapter) {
            GuiButtonChapter chapter = (GuiButtonChapter) button;
            this.mc.displayGuiScreen(new GuiLexiconChapter(chapter.chapter));
        }
        if(button instanceof GuiButtonOptions) {
            
        }
        if(button instanceof GuiButtonAchievement) {
            
        }
        
        if(this.isMainPage(this)) {
            if(button instanceof GuiButtonForward) {
                if(this.maxPages + 1 > this.page)
                    this.page++;
            }
            if(button instanceof GuiButtonBack)
                this.page--;
        }
    }
    
    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        super.keyTyped(typedChar, keyCode);
        
        if(this.mc.gameSettings.keyBindInventory.getKeyCode() == keyCode) {
            this.mc.displayGuiScreen(null);
            this.mc.setIngameFocus();
        }
        
        if(keyCode == GuiLexicon.KONAMI_CODE[this.konamiIndex]) {
            this.konamiIndex++;
            if(this.konamiIndex >= KONAMI_CODE.length) {
                this.konamiIndex = 0;
                this.updateItemName();
            }
        } else this.konamiIndex = 0;
    }
    
    /**
     * Checks if the page is the main page or if it is a sub page.
     * 
     * @param lex the GUI of the page to check
     * @return true when the GUI is the main page
     */
    private boolean isMainPage(GuiLexicon lex) {
        return lex != null && lex.getClass() == GuiLexicon.class;
    }
    
    /**
     * Updates the name of the item the player is
     * currently holding.
     */
    private void updateItemName() {
        NetworkHandler.INSTANCE.sendToServer(new MessageItemNameChanged(GuiLexicon.KONAMI_CODE_NAME));
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }
    
    /**
     * Calculates the maximum scale factor for
     * a default Minecraft GUI. <p>This
     * scale factor <b>has to be recalculated</b>
     * each time the size of the Minecraft window
     * changes!
     * 
     * @return maximum scale factor
     */
    private static int getMaxScale() {
        Minecraft mc = Minecraft.getMinecraft();
        int tmpScale = mc.gameSettings.guiScale;
        mc.gameSettings.guiScale = 0; // reset scale
        ScaledResolution res = new ScaledResolution(mc);
        mc.gameSettings.guiScale = tmpScale;
        
        return res.getScaleFactor();
    }
}