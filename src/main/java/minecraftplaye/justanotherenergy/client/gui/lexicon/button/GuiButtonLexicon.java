/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.client.gui.lexicon.button;

import java.util.List;

import minecraftplaye.justanotherenergy.client.gui.base.ToolTipManager;
import minecraftplaye.justanotherenergy.client.gui.base.ToolTipManager.IToolTipRenderer;
import minecraftplaye.justanotherenergy.client.gui.base.widget.GuiTooltip;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public abstract class GuiButtonLexicon extends GuiButton implements IToolTipRenderer {
    
    private ToolTipManager manager = new ToolTipManager();
    
    public GuiButtonLexicon(int buttonId, int x, int y, int widthIn, int heightIn, String buttonText) {
        super(buttonId, x, y, widthIn, heightIn, buttonText);
    }
    
    @Override
    public void playPressSound(SoundHandler soundHandlerIn) {
        super.playPressSound(soundHandlerIn);
    }
    
    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
        this.manager.drawToolTips(this, mouseX, mouseY);
    }
    
    /**
     * Checks if the mouse is currently hovering over
     * the button.
     * 
     * @param mouseX the position of the mouse on the x axis
     * @param mouseY the position of the mouse on the y axis
     * @return true if the mouse hovers over the button
     */
    public boolean isHoveredOver(int mouseX, int mouseY) {
        return mouseX >= this.x && mouseY >= this.y &&
                mouseX < this.x + this.width &&
                mouseY < this.y + this.height;
    }
    
    /**
     * Adds a tooltip to the GUI.
     * 
     * @param toolTip The tooltip to be added.
     */
    public void addToolTip(GuiTooltip toolTip) {
        this.manager.addToolTip(toolTip);
    }
    
    /**
     * Removes a tooltip from the GUI.
     * 
     * @param toolTip The tooltip to be removed.
     */
    public void removeToolTip(GuiTooltip toolTip) {
        this.manager.removeToolTip(toolTip);
    }
    
    /**
     * Removes all tooltips from the GUI.
     */
    public void resetToolTips() {
        this.manager.reset();
    }
    
    @Override
    public FontRenderer getFontRenderer() {
        return Minecraft.getMinecraft().fontRenderer;
    }
    
    @Override
    public void drawHoveringString(List<String> textLines, int posX, int posY, FontRenderer fontRenderer) {
        if(textLines == null || textLines.isEmpty())
            return;
        
        posY += 300;
        
        GlStateManager.disableLighting();
        GlStateManager.disableDepth();
        
        // search for the longest line to display
        int i = 0; // longest string after for-each
        for(String text : textLines) {
            int length = this.getFontRenderer().getStringWidth(text);
            
            if(length > i)
                i = length;
        }
        
        // positions
        int posLeft = posX + 12;
        int posTop = posY - 12;
        int spacing = 8;
        
        // find best position to place tooltip box in
        if(textLines.size() > 1)
            spacing += 2 + (textLines.size() - 1) * 10;
        if(posLeft + i > this.width)
            posLeft -= 28 + i;
        if(posTop + spacing + 6 > this.height)
            posTop = this.height - spacing - 6;
        
        // draw tooltip box
        this.zLevel = 300.f;
        int color_gray = -267386864;
        int color_blue = 1347420415;
        int color_dark_blue = 1344798847;
        this.drawGradientRect(posLeft - 3, posTop - 4, posLeft + i + 3, posTop - 3, color_gray, color_gray);
        this.drawGradientRect(posLeft - 3, posTop + spacing + 3, posLeft + i + 3, posTop + spacing + 4, color_gray, color_gray);
        this.drawGradientRect(posLeft - 3, posTop - 3, posLeft + i + 3, posTop + spacing + 3, color_gray, color_gray);
        this.drawGradientRect(posLeft - 4, posTop - 3, posLeft - 3, posTop + spacing + 3, color_gray, color_gray);
        this.drawGradientRect(posLeft + i + 3, posTop - 3, posLeft + i + 4, posTop + spacing + 3, color_gray, color_gray);
        
        this.drawGradientRect(posLeft - 3, posTop - 3 + 1, posLeft - 3 + 1, posTop + spacing + 3 - 1, color_blue, color_dark_blue);
        this.drawGradientRect(posLeft + i + 2, posTop - 3 + 1, posLeft + i + 3, posTop + spacing + 3 - 1, color_blue, color_dark_blue);
        this.drawGradientRect(posLeft - 3, posTop - 3, posLeft + i + 3, posTop - 3 + 1, color_blue, color_blue);
        this.drawGradientRect(posLeft - 3, posTop + spacing + 2, posLeft + i + 3, posTop + spacing + 3, color_dark_blue, color_dark_blue);
        
        posTop += 2;
        for(String text : textLines) {
            this.getFontRenderer().drawStringWithShadow(text, posLeft, posTop, -1);
            posTop += 10;
        }
        
        this.zLevel = .0f;
        
        GlStateManager.enableLighting();
        GlStateManager.enableDepth();
    }
}