/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.client.gui.lexicon.button;

import minecraftplaye.justanotherenergy.api.lexicon.LexiconChapter;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiButtonChapter extends GuiButtonLexicon {
    
    public LexiconChapter chapter;
    
    public GuiButtonChapter(int buttonId, int x, int y, int widthIn, int heightIn, LexiconChapter chapter) {
        super(buttonId, x, y, widthIn, heightIn, "");
        this.chapter = chapter;
    }
    
    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
        this.hovered = this.isHoveredOver(mouseX, mouseY);
        int hoverState = this.getHoverState(this.hovered);
        
        int width = this.x + this.width;
        if(hoverState == 2)
            drawRect(this.x - 5, this.y, width, this.y + this.height, 0x33000000);
        
        boolean tmpUnicode = mc.fontRenderer.getUnicodeFlag();
        mc.fontRenderer.setUnicodeFlag(true);
        mc.fontRenderer.drawString(this.displayString, this.x, this.y + (this.height - 8) / 2, 0);
        mc.fontRenderer.setUnicodeFlag(tmpUnicode);
    }
    
    /**
     * Changes the chapter to open when pressing this button.
     * 
     * @param chapter the new chapter to open
     */
    public void changeChapter(LexiconChapter chapter) {
        this.chapter = chapter;
        this.displayString = I18n.format(chapter.getTitle());
    }
}