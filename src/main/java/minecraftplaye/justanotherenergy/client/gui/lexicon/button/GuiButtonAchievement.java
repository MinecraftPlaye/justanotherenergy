/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.client.gui.lexicon.button;

import java.awt.Rectangle;

import minecraftplaye.justanotherenergy.client.gui.base.widget.GuiTooltip;
import minecraftplaye.justanotherenergy.client.gui.lexicon.GuiLexicon;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiButtonAchievement extends GuiButtonLexicon {
    
    private GuiTooltip buttonTooltip;
    
    public GuiButtonAchievement(int buttonId, int x, int y, int widthIn, int heightIn) {
        super(buttonId, x, y, widthIn, heightIn, "");
        this.buttonTooltip = new GuiTooltip(null, I18n.format("")); // TODO:
        this.addToolTip(this.buttonTooltip);
    }
    
    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
        if(this.enabled && this.visible) {
            GlStateManager.color(1.f, 1.f, 1.f, 1.f);
            mc.getTextureManager().bindTexture(GuiLexicon.GUI_BACKGROUND);
            this.drawTexturedModalRect(this.x, this.y, 45, 184, 9, 16);
            
            this.hovered = this.isHoveredOver(mouseX, mouseY);
            if(this.hovered && this.getHoverState(this.hovered) == 2)
                this.drawTexturedModalRect(this.x, this.y, 35, 184, 9, 16);
            
            this.buttonTooltip.setBounds(new Rectangle(mouseX, mouseY + 17, this.buttonTooltip.getToolTipText().size(), mouseY));
        }
    }
}