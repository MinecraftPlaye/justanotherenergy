/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.client.gui.base;

import java.io.IOException;
import java.util.List;

import minecraftplaye.justanotherenergy.client.gui.base.ToolTipManager.IToolTipRenderer;
import minecraftplaye.justanotherenergy.client.gui.base.widget.GuiTooltip;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.inventory.Container;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * A basic container which handles stuff all GUIs might need.
 * 
 * @author MinecraftPlaye
 */
@SideOnly(Side.CLIENT)
public abstract class GuiContainerBase extends GuiContainer implements IToolTipRenderer {
    
    private ToolTipManager manager = new ToolTipManager();
    
    public GuiContainerBase(Container inventorySlotsIn) {
        super(inventorySlotsIn);
    }
    
    @Override
    public void initGui() {
        super.initGui();
    }
    
    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        super.keyTyped(typedChar, keyCode);
    }
    
    @Override
    public void updateScreen() {
        super.updateScreen();
    }
    
    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);
        
        this.manager.drawToolTips(this, mouseX, mouseY);
    }
    
    @Override
    public void handleMouseInput() throws IOException {
        super.handleMouseInput();
    }
    
    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        super.drawGuiContainerForegroundLayer(mouseX, mouseY);
    }
    
    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
    }
    
    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
    }
    
    /**
     * Adds a tooltip to the GUI.
     * 
     * @param toolTip The tooltip to be added.
     */
    public void addToolTip(GuiTooltip toolTip) {
        this.manager.addToolTip(toolTip);
    }
    
    /**
     * Removes a tooltip from the GUI.
     * 
     * @param toolTip The tooltip to be removed.
     */
    public void removeToolTip(GuiTooltip toolTip) {
        this.manager.removeToolTip(toolTip);
    }
    
    /**
     * Removes all tooltips from the GUI.
     */
    public void resetToolTips() {
        this.manager.reset();
    }
    
    /**
     * Draws a string in a specific size.
     * 
     * @param text The text to draw.
     * @param x The x position to draw the text to.
     * @param y The y position to draw the text to.
     * @param scale The size of the text. <i>1</i> is the full size.
     * @param color The color the text should be drawn in.
     */
    public void drawStringScaled(String text, int x, int y, float scale, int color) {
        GlStateManager.pushMatrix();
        GlStateManager.scale(.6f, .6f, .6f);
        this.getFontRenderer().drawString(text, x, y, color);
        GlStateManager.popMatrix();
    }
    
    @Override
    public FontRenderer getFontRenderer() {
        return this.fontRenderer;
    }
    
    @Override
    public void drawHoveringString(List<String> textLines, int posX, int posY, FontRenderer fontRenderer) {
        if(textLines == null || textLines.isEmpty())
            return;
        
        GlStateManager.disableLighting();
        GlStateManager.disableDepth();
        
        // search for the longest line to display
        int i = 0; // longest string after for-each
        for(String text : textLines) {
            int length = this.getFontRenderer().getStringWidth(text);
            
            if(length > i)
                i = length;
        }
        
        // positions
        int posLeft = posX + 12;
        int posTop = posY - 12;
        int spacing = 8;
        
        // find best position to place tooltip box in
        if(textLines.size() > 1)
            spacing += 2 + (textLines.size() - 1) * 10;
        if(posLeft + i > this.width)
            posLeft -= 28 + i;
        if(posTop + spacing + 6 > this.height)
            posTop = this.height - spacing - 6;
        
        // draw tooltip box
        this.zLevel = 300.f;
        int color_gray = -267386864;
        int color_blue = 1347420415;
        int color_dark_blue = 1344798847;
        this.drawGradientRect(posLeft - 3, posTop - 4, posLeft + i + 3, posTop - 3, color_gray, color_gray);
        this.drawGradientRect(posLeft - 3, posTop + spacing + 3, posLeft + i + 3, posTop + spacing + 4, color_gray, color_gray);
        this.drawGradientRect(posLeft - 3, posTop - 3, posLeft + i + 3, posTop + spacing + 3, color_gray, color_gray);
        this.drawGradientRect(posLeft - 4, posTop - 3, posLeft - 3, posTop + spacing + 3, color_gray, color_gray);
        this.drawGradientRect(posLeft + i + 3, posTop - 3, posLeft + i + 4, posTop + spacing + 3, color_gray, color_gray);
        
        this.drawGradientRect(posLeft - 3, posTop - 3 + 1, posLeft - 3 + 1, posTop + spacing + 3 - 1, color_blue, color_dark_blue);
        this.drawGradientRect(posLeft + i + 2, posTop - 3 + 1, posLeft + i + 3, posTop + spacing + 3 - 1, color_blue, color_dark_blue);
        this.drawGradientRect(posLeft - 3, posTop - 3, posLeft + i + 3, posTop - 3 + 1, color_blue, color_blue);
        this.drawGradientRect(posLeft - 3, posTop + spacing + 2, posLeft + i + 3, posTop + spacing + 3, color_dark_blue, color_dark_blue);
        
        posTop += 2;
        for(String text : textLines) {
            this.getFontRenderer().drawStringWithShadow(text, posLeft, posTop, -1);
            posTop += 10;
        }
        
        this.zLevel = .0f;
        
        GlStateManager.enableLighting();
        GlStateManager.enableDepth();
    }
}