/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.client.gui.base.widget;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import minecraftplaye.justanotherenergy.api.gui.IHideable;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * A Tooltip Widget to be shown in the Minecraft GUI.
 * 
 * @author MinecraftPlaye
 */
@SideOnly(Side.CLIENT)
public class GuiTooltip implements IHideable {
	
	private long mouseOverStart;
	
	/**
	 * The size of the tooltip.
	 */
	@Nullable
	private Rectangle bounds;
	
	/**
	 * The last position of the mouse on the x-axis.
	 */
	private int lastMouseX = -1;
	
	/**
	 * The last position of the mouse on the y-axis.
	 */
	private int lastMouseY = -1;
	
	/**
	 * The text to display. Each object in the list equals
	 * one line in the tooltip.
	 */
	@Nonnull
	private final List<String> text;
	
	/**
	 * Determines if this tooltip is visible or not.
	 */
	private boolean visible = true;
	
	/**
	 * Constructor for setting the basic values.
	 * Each String argument equals one line in the
	 * tooltip.
	 * 
	 * @param bounds The size of the tooltip.
	 * @param lines The text to display in the tooltip.
	 */
	public GuiTooltip(@Nullable Rectangle bounds, @Nullable String... lines) {
		this.bounds = bounds;
		if(lines != null) {
			this.text = new ArrayList<String>(lines.length);
			for(String line : lines)
				this.text.add(line);
		} else
			this.text = new ArrayList<String>();
	}
	
	/**
	 * Constructor for setting the basic values.
	 * Each String in the list equals one line in the
	 * tooltip.
	 * 
	 * @param bounds The size of the tooltip.
	 * @param lines The text to display in the tooltip.
	 */
	public GuiTooltip(@Nullable Rectangle bounds, @Nullable List<String> lines) {
		this.bounds = bounds;
		if(lines == null)
			this.text = new ArrayList<String>();
		else
			this.text = new ArrayList<String>(lines);
	}
	
	/**
	 * Updates the tooltip logic.
	 * 
	 * @param mouseX The x position of the mouse.
	 * @param mouseY The y position of the mouse.
	 */
	public void update(int mouseX, int mouseY) {
	    if(this.lastMouseX != mouseX || this.lastMouseY != mouseY)
		    this.mouseOverStart = 0;
	    
		if(this.isVisible() && this.getBounds() != null && this.getBounds().contains(mouseX, mouseY)) {
		    if(this.mouseOverStart == 0)
			    this.mouseOverStart = System.currentTimeMillis();
		} else
		    this.mouseOverStart = 0;
		
		this.lastMouseX = mouseX;
		this.lastMouseY = mouseY;
	}
	
	/**
	 * Determines if the tooltip should and can be drawn or not.
	 * @return Returns true if the tooltip should be drawn.
	 */
	public boolean canDraw() {
		if(!this.isVisible())
			return false;
		if(this.mouseOverStart == 0)
			return false;
		return System.currentTimeMillis() - this.mouseOverStart >= 0;
	}
	
	@Override
	public void setIsVisible(boolean visible) {
		this.visible = visible;
	}
	
	@Override
	public boolean isVisible() {
		return this.visible;
	}
	
	/**
	 * Returns the size of the tooltip.
	 * @return the size as a Rectangle object
	 */
	@Nullable
	public Rectangle getBounds() {
		return this.bounds;
	}
	
	/**
	 * Sets the size of the tooltip.
	 * 
	 * @param bounds The size of the tooltip.
	 */
	public void setBounds(@Nullable Rectangle bounds) {
		this.bounds = bounds;
	}
	
	/**
	 * Clears the current tooltip text and sets
	 * the new tooltip text. Each argument equals one
	 * line of text in the tooltip.
	 * 
	 * @param txt The text to display.
	 */
	public void setToolTipText(@Nullable String... txt) {
		this.text.clear();
		if(txt != null) {
			for(String line : txt) 
				this.text.add(line);
		}
	}
	
	/**
	 * Returns the text of the tooltip.
	 * @return the text as a list
	 */
	@Nonnull
	public List<String> getToolTipText() {
		return this.text;
	}
}