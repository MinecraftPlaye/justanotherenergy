/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.client.gui.base;

import java.util.ArrayList;
import java.util.List;

import minecraftplaye.justanotherenergy.client.gui.base.widget.GuiTooltip;

import net.minecraft.client.gui.FontRenderer;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * A helper class for rendering tooltips.
 * This class holds a list with all tooltips to
 * render and methods to render them onto the
 * screen.
 * 
 * @author MinecraftPlaye
 */
@SideOnly(Side.CLIENT)
public class ToolTipManager {
	
	/**
	 * A list which holds all tooltips.
	 */
	private final List<GuiTooltip> toolTips = new ArrayList<GuiTooltip>();
	
	/**
	 * Renders all tooltips to the screen.
	 * 
	 * @param renderer The GUI to render the tooltips to.
	 * @param mouseX The x position of the mouse.
	 * @param mouseY The y position of the mouse.
	 */
	public final void drawToolTips(IToolTipRenderer renderer, int mouseX, int mouseY) {
		for(GuiTooltip toolTip : this.toolTips) {
			toolTip.update(mouseX, mouseY);
			if(toolTip.canDraw())
				this.drawToolTip(toolTip, renderer, mouseX, mouseY);
		}
	}
	
	/**
	 * Renders a specific tooltip to the screen.
	 * 
	 * @param toolTip The tooltip to render.
	 * @param renderer The GUI to render the tooltips to.
	 * @param mouseX The x position of the mouse.
	 * @param mouseY The y position of the mouse.
	 */
	private void drawToolTip(GuiTooltip toolTip, IToolTipRenderer renderer, int mouseX, int mouseY) {
		List<String> text = toolTip.getToolTipText();
		if(text == null) return;
		
		for(int i = 0; i < text.size(); i++) {
			if(i == 0)
				text.get(i).replace(text.get(i), "\u00A7f" + text.get(i));
			else
				text.get(i).replace(text.get(i), "\u00a77" + text.get(i));
		}
		
		renderer.drawHoveringString(text, mouseX, mouseY, renderer.getFontRenderer());
	}
	
	/**
	 * Adds a tooltip to the list.
	 * 
	 * @param toolTip The tooltip to be added.
	 */
	public void addToolTip(GuiTooltip toolTip) {
		this.toolTips.add(toolTip);
	}
	
	/**
	 * Removes a tooltip from the list.
	 * 
	 * @param toolTip The tooltip to be removed.
	 */
	public void removeToolTip(GuiTooltip toolTip) {
		this.toolTips.remove(toolTip);
	}
	
	/**
	 * Deletes all Tooltips from the list.
	 */
	public void reset() {
		this.toolTips.clear();
	}
	
	/**
	 * {@link IToolTipRenderer} provides access of GUI methods to
	 * classes outside of the default Minecraft GUI system to save
	 * performance and RAM.
	 * 
	 * @author ShadowDragon
	 */
	@SideOnly(Side.CLIENT)
	public static interface IToolTipRenderer {
		
		/**
		 * Returns the font renderer object used by Minecraft.
		 * @return the font renderer
		 */
		FontRenderer getFontRenderer();
		
		/**
		 * Draws a List of strings as a tooltip. Every entry is drawn on a seperate line.
		 * 
		 * @param textLines The text to draw.
		 * @param posX The x position to draw at.
		 * @param posY The y position to draw at.
		 * @param fontRenderer The FontRenderer used to draw the text with a specific font.
		 */
		void drawHoveringString(List<String> textLines, int posX, int posY, FontRenderer fontRenderer);
	}
}