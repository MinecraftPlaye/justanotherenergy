/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.client.gui.base;

import minecraftplaye.justanotherenergy.api.gui.IHideable;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * A GUI button which has the option to not show it
 * on the GUI or show it, depending on the user's
 * actions.
 * 
 * @author MinecraftPlaye
 */
@SideOnly(Side.CLIENT)
public class GuiButtonHideable extends GuiButton implements IHideable {
	
	public GuiButtonHideable(int buttonId, int x, int y, String buttonText) {
		super(buttonId, x, y, buttonText);
	}
	
	public GuiButtonHideable(int buttonId, int x, int y, int widthIn, int heightIn, String buttonText) {
		super(buttonId, x, y, widthIn, heightIn, buttonText);
	}
	
	@Override
	public void setIsVisible(boolean visible) {
		this.visible = visible;
	}
	
	@Override
	public boolean isVisible() {
		return this.visible;
	}
	
	/**
	 * Enables or deactivates the button.
	 * 
	 * @param enabled Is the button enabled?
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	/**
	 * Returns true if this button is enabled. 
	 * @return whether the button is enabled
	 */
	public boolean isEnabled() {
		return this.enabled;
	}
	
	@Override
	protected int getHoverState(boolean mouseOver) {
		int hoveringState = super.getHoverState(mouseOver);
		if(!this.isEnabled())
			hoveringState = 0;
		
		return hoveringState;
	}
	
	@Override
	public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
		return this.isEnabled() && super.mousePressed(mc, mouseX, mouseY);
	}
}
