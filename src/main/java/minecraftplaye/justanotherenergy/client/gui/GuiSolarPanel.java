/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.client.gui;

import java.awt.Rectangle;

import minecraftplaye.justanotherenergy.JustAnotherEnergy;
import minecraftplaye.justanotherenergy.client.gui.base.GuiContainerBase;
import minecraftplaye.justanotherenergy.client.gui.base.widget.GuiTooltip;
import minecraftplaye.justanotherenergy.common.container.ContainerSolarPanel;
import minecraftplaye.justanotherenergy.common.lib.Constants;
import minecraftplaye.justanotherenergy.common.lib.ModConfig;
import minecraftplaye.justanotherenergy.common.tileentities.TileEntitySolarPanel;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * @deprecated Replaced with the IRenderOverlay system. Look
 * at {@link net.minecraftforge.client.event.RenderGameOverlayEvent.Post RenderGameOverlayEvent.Post} and {@link UIEventHandler}.
 * 
 * @author MinecraftPlaye
 */
@Deprecated
@SideOnly(Side.CLIENT)
public class GuiSolarPanel extends GuiContainerBase {
	
	private final InventoryPlayer playerInv;
	private final TileEntitySolarPanel panel;
	private GuiTooltip storedEnergyToolTip;
	
	private static final ResourceLocation SOLAR_PANEL_TEXTURE = new ResourceLocation(Constants.MOD_ID.toLowerCase() + ":" + "textures/gui/container/solar_panel.png");
	
	public GuiSolarPanel(InventoryPlayer playerInv, TileEntitySolarPanel panel) {
		super(new ContainerSolarPanel(playerInv, panel));
		this.playerInv = playerInv;
		this.panel = panel;
	}
	
	@Override
	public void initGui() {
		super.initGui();
		int guiStartX = (this.width - this.xSize) / 2;
		int guiStartY = (this.height - this.ySize) / 2;
		
		this.storedEnergyToolTip = new GuiTooltip(new Rectangle(guiStartX + 16, guiStartY + 17, 16, 51));
		this.addToolTip(this.storedEnergyToolTip);
	}
	
	@Override
	public void updateScreen() {
		super.updateScreen();
		
		if(!this.storedEnergyToolTip.isVisible() && this.panel.getEnergyManager().getEnergyStored() > 0)
			this.storedEnergyToolTip.setIsVisible(true);
		else if(this.panel.getEnergyManager().getEnergyStored() <= 0)
			this.storedEnergyToolTip.setIsVisible(false);
			
		this.storedEnergyToolTip.setToolTipText(this.panel.getEnergyManager().getEnergyStored() + "/" + this.panel.getEnergyManager().getDisplayMaxStored() + " " + ModConfig.ENERGY_NAME);
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		super.drawGuiContainerForegroundLayer(mouseX, mouseY);
		String title = JustAnotherEnergy.proxy.localize("tile.justanotherenergy.solar_panel.name");
		
		this.getFontRenderer().drawString(title, this.xSize / 2 - this.getFontRenderer().getStringWidth(title) / 2, 6, 4210752);
		this.getFontRenderer().drawString(this.playerInv.getDisplayName().getUnformattedText(), 8, this.ySize - 94, 4210752);
		
		this.drawStringScaled(JustAnotherEnergy.proxy.localize("gui.solar_panel.energy_stored.name") + ": " + this.panel.getEnergyManager().getEnergyStored() + "/" + this.panel.getEnergyManager().getDisplayMaxStored() + " " + ModConfig.ENERGY_NAME, this.xSize - 100, this.ySize - 130, .6f, 4210752);
		this.drawStringScaled(JustAnotherEnergy.proxy.localize("gui.solar_panel.input_rate.name") + ": " + this.panel.getEnergyManager() + " " + Constants.FORGE_ENERGY_T, this.xSize - 100, this.ySize - 110, .6f, 4210752);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		super.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
		
		GlStateManager.color(1.f, 1.f, 1.f, 1.f);
		
		// Draw texture
		this.mc.getTextureManager().bindTexture(SOLAR_PANEL_TEXTURE);
		int guiStartX = (this.width - this.xSize) / 2;
		int guiStartY = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(guiStartX, guiStartY, 0, 0, this.xSize, this.ySize);
		
		int progressBar = this.getCapacityLeftScaled(51);
		this.drawTexturedModalRect(guiStartX + 16, guiStartY + 17 + 51 - progressBar, 176, 51 - progressBar, 16, progressBar + 1);
	}
	
	private int getCapacityLeftScaled(int pixels) {
		int stored = this.panel.getEnergyManager().getEnergyStored();
		int capacity = this.panel.getEnergyManager().getDisplayMaxStored();
		
		return stored != 0 && capacity != 0 ? stored * pixels / capacity : 0;
	}
}
