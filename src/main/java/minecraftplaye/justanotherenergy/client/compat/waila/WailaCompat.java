package minecraftplaye.justanotherenergy.client.compat.waila;

import minecraftplaye.justanotherenergy.common.lib.Constants;
import minecraftplaye.justanotherenergy.common.tileentities.TileEntityEnergy;

import net.minecraftforge.fml.common.Optional;

import mcp.mobius.waila.api.IWailaPlugin;
import mcp.mobius.waila.api.IWailaRegistrar;
import mcp.mobius.waila.api.WailaPlugin;

@Optional.InterfaceList({ 
    @Optional.Interface(iface = "mcp.mobius.waila.api.WailaPlugin", modid = Constants.MODID_WAILA),
    @Optional.Interface(iface = "mcp.mobius.waila.api.IWailaPlugin", modid = Constants.MODID_WAILA)
})
@WailaPlugin(Constants.MOD_ID)
public class WailaCompat implements IWailaPlugin {
    
    @Override
    public void register(IWailaRegistrar registrar) {
        registrar.registerBodyProvider(HUDWailaEnergy.INSTANCE, TileEntityEnergy.class);
        registrar.registerNBTProvider(HUDWailaEnergy.INSTANCE, TileEntityEnergy.class);
    }
}