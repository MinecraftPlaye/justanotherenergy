/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.client.compat.waila;

import java.util.List;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import mcp.mobius.waila.api.IWailaDataProvider;

import minecraftplaye.justanotherenergy.common.lib.Constants;
import minecraftplaye.justanotherenergy.common.lib.ModConfig;
import minecraftplaye.justanotherenergy.common.tileentities.TileEntityEnergy;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import net.minecraftforge.fml.common.Optional;

/**
 * Waila compatibility.
 * 
 * @author MinecraftPlaye
 */
@Optional.Interface(iface = "mcp.mobius.waila.api.IWailaDataProvider", modid = Constants.MODID_WAILA)
public class HUDWailaEnergy implements IWailaDataProvider {
    
    public static final HUDWailaEnergy INSTANCE = new HUDWailaEnergy();
    
    @Override
    public ItemStack getWailaStack(IWailaDataAccessor accessor, IWailaConfigHandler config) {
        return ItemStack.EMPTY;
    }
    
    @Override
    public List<String> getWailaHead(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor,
            IWailaConfigHandler config) {
        return currenttip;
    }
    
    @Override
    public List<String> getWailaBody(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor,
            IWailaConfigHandler config) {
        if (config.getConfig("capability.energyinfo") || accessor.getTileEntity() == null)
            return currenttip;
        
        if(accessor.getNBTData().hasKey("forgeEnergy")) {
            NBTTagCompound energyTag = accessor.getNBTData().getCompoundTag("forgeEnergy");
            int stored = energyTag.getInteger("stored");
            
            int capacity = 0;
            if(!energyTag.hasKey("dpCapacity")) capacity = energyTag.getInteger("capacity");
            else capacity = energyTag.getInteger("dpCapacity");
            
            currenttip.add(String.format("%s%s%s / %s%s%s %s", TextFormatting.WHITE, stored, TextFormatting.RESET,
                    TextFormatting.WHITE, capacity, TextFormatting.RESET, ModConfig.ENERGY_NAME));
        }
        
        return currenttip;
    }
    
    @Override
    public List<String> getWailaTail(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor,
            IWailaConfigHandler config) {
        return currenttip;
    }
    
    @Override
    public NBTTagCompound getNBTData(EntityPlayerMP player, TileEntity te, NBTTagCompound tag, World world,
            BlockPos pos) {
        if(te != null && te instanceof TileEntityEnergy) {
            TileEntityEnergy tileEnergy = (TileEntityEnergy) te;
            
            if(tileEnergy.container != null) {
                NBTTagCompound energyTag = new NBTTagCompound();
                energyTag.setInteger("capacity", tileEnergy.container.getMaxEnergyStored());
                energyTag.setInteger("stored", tileEnergy.container.getEnergyStored());
                
                if(tileEnergy.shouldExplode)
                    energyTag.setInteger("dpCapacity", tileEnergy.container.getDisplayMaxStored());
                
                tag.setTag("forgeEnergy", energyTag);
            }
        }
        return tag;
    }
}