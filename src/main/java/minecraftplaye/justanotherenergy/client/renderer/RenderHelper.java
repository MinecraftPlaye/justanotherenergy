/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.client.renderer;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.util.ResourceLocation;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import static org.lwjgl.opengl.GL11.*;

@SideOnly(Side.CLIENT)
public class RenderHelper {
	
	public static final ResourceLocation BLOCK_TEXTURE = TextureMap.LOCATION_BLOCKS_TEXTURE;
	
	/**
	 * Returns the RenderEngine instance used by Minecraft.
	 * @return the RenderEngine instance
	 */
	public static TextureManager getEngine() {
		return Minecraft.getMinecraft().getTextureManager();
	}
	
	/**
	 * Returns the font renderer used for displaying and measuring text.
	 * @return the font renderer
	 */
	public static FontRenderer getFontRenderer() {
		return Minecraft.getMinecraft().fontRenderer;
	}
	
	/**
	 * Binds the texture of the texture block atlas.
	 */
	public static void bindItemTexture() {
		RenderHelper.getEngine().bindTexture(RenderHelper.BLOCK_TEXTURE);
	}
	
	/**
	 * Binds the texture.
	 * 
	 * @param location the path to the texture to bind including the texture and format
	 */
	public static void bindTexture(String location) {
		RenderHelper.getEngine().bindTexture(new ResourceLocation(location));
	}
	
	/**
	 * Binds the texture.
	 * 
	 * @param location the path to the texture to bind including the texture and format
	 */
	public static void bindTexture(ResourceLocation location) {
		RenderHelper.getEngine().bindTexture(location);
	}
	
	/**
	 * Renders a rectangular texture to the screen.
	 * 
	 * @param x the x position of the screen to render at
	 * @param y the y position of the screen to render at
	 * @param z the z position of the screen to render at
	 * @param textureX the position of the texture
	 * @param textureY the position of the texture
	 * @param width the width of the texture
	 * @param height the height of the texture
	 */
	public static void renderTexturedRect(int x, int y, int z, int textureX, int textureY, int width, int height) {
	    glBegin(GL_QUADS);
	    
	    glTexCoord2f(textureX * (1.f / 256.f), textureY * (1.f / 256.f));
	    glVertex3i(x, y, z);
	    
	    glTexCoord2f(textureX * (1.f / 256.f), (textureY + height) * (1.f / 256.f));
	    glVertex3i(x, y + height, z);
	    
	    glTexCoord2f((textureX + width) * (1.f / 256.f), (textureY + height) * (1.f / 256.f));
	    glVertex3i(x + width, y + height, z);
	    
	    glTexCoord2f((textureX + width) * (1.f / 256.f), textureY * (1.f / 256.f));
	    glVertex3i(x + width, y, z);
	    
	    glEnd();
	}
	
	/**
	 * Renders a rectangular texture to the screen.
	 * 
	 * @param x the x position of the screen to render at
	 * @param y the y position of the screen to render at
	 * @param z the z position of the screen to render at
	 * @param textureX the position of the texture
	 * @param textureY the position of the texture
	 * @param width the width of the texture
	 * @param height the height of the texture
	 * @param scale the scale to draw the texture
	 */
	public static void renderSizedTexturedRect(int x, int y, int z, int textureX, int textureY, int width, int height, float scale) {
	    glBegin(GL_QUADS);
	    
	    glTexCoord2f(textureX * (1.f / 256.f), textureY * (1.f / 256.f));
	    glVertex3i(x, y, z);
	    
	    glTexCoord2f(textureX * (1.f / 256.f), (textureY + height) * (1.f / 256.f));
	    glVertex3f(x, y + (height * scale), z);
	    
	    glTexCoord2f((textureX + width) * (1.f / 256.f), (textureY + height) * (1.f / 256.f));
	    glVertex3f(x + (width * scale), y + (height * scale), z);
	    
	    glTexCoord2f((textureX + width) * (1.f / 256.f), textureY * (1.f / 256.f));
	    glVertex3f(x + (width * scale), y, z);
	    
	    glEnd();
	}
	    
    /**
     * Renders a rectangular texture to the screen.
     * 
     * @param x the x position of the screen to render at
     * @param y the y position of the screen to render at
     * @param z the z position of the screen to render at
     * @param textureX the position of the texture
     * @param textureY the position of the texture
     * @param width the width of the texture
     * @param height the height of the texture
     * @param drawHeight the height to draw the texture with
     * @param drawWidth the width to draw the texture with
     */
	public static void renderSizedTexturedRect(int x, int y, int z, int textureX, int textureY, int width, int height, int drawWidth, int drawHeight) {
        glBegin(GL_QUADS);
        
        glTexCoord2f(textureX * (1.f / 256.f), textureY * (1.f / 256.f));
        glVertex3i(x, y, z);
        
        glTexCoord2f(textureX * (1.f / 256.f), (textureY + height) * (1.f / 256.f));
        glVertex3i(x, y + drawHeight, z);
        
        glTexCoord2f((textureX + width) * (1.f / 256.f), (textureY + height) * (1.f / 256.f));
        glVertex3i(x + drawWidth, y + drawHeight, z);
        
        glTexCoord2f((textureX + width) * (1.f / 256.f), textureY * (1.f / 256.f));
        glVertex3i(x + drawWidth, y, z);
        
        glEnd();
    }
}