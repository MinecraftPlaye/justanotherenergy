/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy;

import minecraftplaye.justanotherenergy.common.events.GrowthEvents;
import minecraftplaye.justanotherenergy.common.events.PlayerEvents;
import minecraftplaye.justanotherenergy.common.power.pollution.CapabilityPollution;

import net.minecraftforge.common.MinecraftForge;

public class ServerProxy implements IProxy {
	
    @Override
    public void registerEventHooks() {
        MinecraftForge.EVENT_BUS.register(new PlayerEvents());
        MinecraftForge.EVENT_BUS.register(new CapabilityPollution());
        MinecraftForge.EVENT_BUS.register(new GrowthEvents());
    }
    
	@Override
	public String localize(String unlocalized, Object... args) {
		return unlocalized;
	}
}
