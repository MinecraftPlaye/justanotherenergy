/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.container;

import minecraftplaye.justanotherenergy.common.tileentities.TileEntitySolarPanel;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.Slot;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContainerSolarPanel extends Container {
	
	private int energyStored;
	private int productionAmount;
	private TileEntitySolarPanel panel;
	
	public ContainerSolarPanel(InventoryPlayer playerInv, TileEntitySolarPanel panel) {
		this.panel = panel;
		
		for(int y = 0; y < 3; ++y)
			for(int x = 0; x < 9; ++x)
				this.addSlotToContainer(new Slot(playerInv, x + y * 9 + 9, 8 + x * 18, 84 + y * 18));
		
		for(int x = 0; x < 9; ++x)
			this.addSlotToContainer(new Slot(playerInv, x, 8 + x * 18, 142));
	}
	
	@Override
	public void detectAndSendChanges() {
		super.detectAndSendChanges();
		
		for(int i = 0; i < this.listeners.size(); i++) {
			IContainerListener listener = (IContainerListener) this.listeners.get(i);
			
			if(this.energyStored != this.panel.getField(0))
				listener.sendWindowProperty(this, 0, this.panel.getField(0));
			if(this.productionAmount != this.panel.getField(1))
				listener.sendWindowProperty(this, 1, this.panel.getField(1));
		}
		
		this.energyStored = this.panel.getField(0);
		this.productionAmount = this.panel.getField(1);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int id, int data) {
		super.updateProgressBar(id, data);
		this.panel.setField(id, data);
	}
	
    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return this.panel.isUsableByPlayer(playerIn);
    }
}
