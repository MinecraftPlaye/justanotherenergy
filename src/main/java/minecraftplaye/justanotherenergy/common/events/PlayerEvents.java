/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.events;

import minecraftplaye.justanotherenergy.JustAnotherEnergy;
import minecraftplaye.justanotherenergy.common.lib.Constants;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

public class PlayerEvents {
    
    @SubscribeEvent
    public void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event) {
        if(!JustAnotherEnergy.successSign) {
            event.player.sendMessage(new TextComponentString(TextFormatting.DARK_GREEN + "[" + Constants.MOD_NAME + "] " + TextFormatting.DARK_RED + 
                    "The signature of the mod is invalid." + TextFormatting.BOLD + " This version will " + TextFormatting.UNDERLINE + 
                    "NOT" + TextFormatting.RESET + TextFormatting.DARK_RED + TextFormatting.BOLD +" receive support." + 
                    TextFormatting.RESET + TextFormatting.DARK_RED + " Proceed at your own risk."));
        }
    }
}