/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.events;

import net.minecraftforge.event.terraingen.SaplingGrowTreeEvent;
import net.minecraftforge.event.world.BlockEvent.CropGrowEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class GrowthEvents {
    
    @SubscribeEvent
    public void onCropGrowing(CropGrowEvent.Pre event) {
        
    }
    
    @SubscribeEvent
    public void onCropGrowed(CropGrowEvent.Post event) {
        
    }
    
    @SubscribeEvent
    public void onSaplingGrowing(SaplingGrowTreeEvent event) {
        
    }
}