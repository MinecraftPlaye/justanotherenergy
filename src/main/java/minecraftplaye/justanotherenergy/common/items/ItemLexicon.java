/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.items;

import javax.annotation.Nonnull;

import minecraftplaye.justanotherenergy.GuiHandler;
import minecraftplaye.justanotherenergy.JustAnotherEnergy;
import minecraftplaye.justanotherenergy.api.lexicon.IKnowledge;
import minecraftplaye.justanotherenergy.api.lexicon.KnowledgeType;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemLexicon extends BasicItem implements IKnowledge {
    
    private static final String TAG_KNOWLEDGE_PREFIX = "knowledge.";
    
    public ItemLexicon() {
        super();
        this.setMaxStackSize(1);
    }
    
    @Nonnull
    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        playerIn.openGui(JustAnotherEnergy.getInstance(), GuiHandler.GUI_ID_LEXICON, worldIn, 0, 0, 0);
        return super.onItemRightClick(worldIn, playerIn, handIn);
    }
    
    @Override
    public EnumRarity getRarity(ItemStack stack) {
        return EnumRarity.EPIC;
    }
    
    @Override
    public boolean isKnowledgeUnlocked(KnowledgeType type, ItemStack stack) {
        if(stack == null) return false;
        
        if(!stack.hasTagCompound())
            stack.setTagCompound(new NBTTagCompound());
        
        if(stack.getTagCompound().hasKey(TAG_KNOWLEDGE_PREFIX + type.getID()))
            return stack.getTagCompound().getBoolean(TAG_KNOWLEDGE_PREFIX + type.getID());
        
        return false;
    }
    
    @Override
    public void unlockKnowledge(KnowledgeType type, ItemStack stack) {
        if(stack == null) return;
        
        if(!stack.hasTagCompound())
            stack.setTagCompound(new NBTTagCompound());
        
        stack.getTagCompound().setBoolean(TAG_KNOWLEDGE_PREFIX + type.getID(), true);
    }
}