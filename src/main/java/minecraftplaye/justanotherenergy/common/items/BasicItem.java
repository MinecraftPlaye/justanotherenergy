/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.items;

import minecraftplaye.justanotherenergy.common.lib.ModCreativeTabs;

import net.minecraft.item.Item;

public class BasicItem extends Item {
    
    public BasicItem() {
        this.setCreativeTab(ModCreativeTabs.mcpowerTab);
    }
}