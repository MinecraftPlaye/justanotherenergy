/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.lib;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModCreativeTabs {
	
	public static final CreativeTabs mcpowerTab = new CreativeTabs(CreativeTabs.getNextID(), Constants.MOD_ID.toLowerCase()) {
		
		@Override
		public String getBackgroundImageName() {
			return "item_search.png";
		}
		
		@Override
		@SideOnly(Side.CLIENT)
		public ItemStack getTabIconItem() {
			return new ItemStack(Items.ACACIA_BOAT);
		}
		
		@Override
		@SideOnly(Side.CLIENT)
		public String getTranslatedTabLabel() {
			return Constants.MOD_NAME;
		}
		
		@Override
		public boolean hasSearchBar() {
			return true;
		}
	};
}