/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.lib;

import minecraftplaye.justanotherenergy.common.items.*;

import net.minecraft.item.Item;

import net.minecraftforge.oredict.OreDictionary;

public class ModItems {
	
	// pre-industrial age
	public static Item sledgehammer;
	public static Item snip;
	
	// general
	public static Item ingot_copper;
	public static Item ingot_tin;
	public static Item plate_copper;
	public static Item plate_tin;
	public static Item capacitor;
	
    public static Item lexicon;
    public static Item silicium;
    public static Item siliciumWafer;
    
	public static void init() {
		// pre-industrial age
		ModItems.sledgehammer = ItemRegistry.register(new BasicItem(), "sledgehammer");
		ModItems.snip = ItemRegistry.register(new BasicItem(), "snip");
		
		// general
		ModItems.ingot_copper = ItemRegistry.register(new BasicItem(), "ingot_copper");
		ModItems.ingot_tin = ItemRegistry.register(new BasicItem(), "ingot_tin");
		ModItems.plate_copper = ItemRegistry.register(new BasicItem(), "plate_copper");
		ModItems.plate_tin = ItemRegistry.register(new BasicItem(), "plate_tin");
		ModItems.capacitor = ItemRegistry.register(new BasicItem(), "capacitor");
		
		//ModItems.lexicon = ItemRegistry.register(new ItemLexicon(), "lexicon");
		//ModItems.silicium = ItemRegistry.register(new ItemSilicium(), "silicium");
		//ModItems.siliciumWafer = ItemRegistry.register(new ItemSiliciumWafer(), "silicium_wafer");
		
		OreDictionary.registerOre("ingotCopper", ModItems.ingot_copper);
		OreDictionary.registerOre("ingotTin", ModItems.ingot_tin);
		OreDictionary.registerOre("plateCopper", ModItems.plate_copper);
		OreDictionary.registerOre("plateTin", ModItems.plate_tin);
	}
}