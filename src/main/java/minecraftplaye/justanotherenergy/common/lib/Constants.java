/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.lib;

import net.minecraft.launchwrapper.Launch;

public class Constants {
    
    public static final String MOD_ID			     = "justanotherenergy";
    public static final String MOD_NAME 	         = "JustAnotherEnergy";
    public static final String VERSION 			     = "@VERSION@";
    public static final String BUILD				 = "@BUILD@";
    public static final String UPDATE				 = "https://bitbucket.org/MinecraftPlaye/primeval-forest-public/raw/master/update.json";
    public static final String FINGERPRINT           = "871d7cb8b8adb446d6288d939be648adbf3fad38";
    
    public static final String MC_VERSION			 = "[1.11.2]";
    public static final String FORGE_VERSION		 = "required-after:forge@[13.20.1.2454,);";
    
    public static final String DEPENDENCIES          = "after:waila@[1.8.17,);after:jei@[4.5,);after:theoneprobe@[1.4.18,);";
    
    public static final String CLIENT_PROXY_LOCATION = "minecraftplaye.justanotherenergy.ClientProxy";
    public static final String SERVER_PROXY_LOCATION = "minecraftplaye.justanotherenergy.ServerProxy";
    
    public static final boolean DEVELOPMENTENVIRONMENT = (Boolean)Launch.blackboard.get("fml.deobfuscatedEnvironment");
    
    public static final String MODID_TESLA = "tesla";
    public static final String MODID_NEI = "nei";
    public static final String MODID_WAILA = "waila";
    
    public static String FORGE_ENERGY_T = ModConfig.ENERGY_NAME + "/t";
}