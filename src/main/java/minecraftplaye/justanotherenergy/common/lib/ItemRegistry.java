/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.lib;

import net.minecraft.item.Item;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * The <code>ItemRegistry</code> class contains several useful class
 * methods to register Items. It cannot be instantiated.
 * 
 * @author MinecraftPlaye
 */
public class ItemRegistry {
    
    public static Item register(Item item, String name) {
        return ItemRegistry.register(item, name, false);
    }
    
    public static Item register(Item item, String name, boolean withMeta) {
        item.setUnlocalizedName(Constants.MOD_ID.toLowerCase() + "." + name);
        item.setRegistryName(name);
        GameRegistry.register(item);
        
        if(FMLCommonHandler.instance().getEffectiveSide().isClient())
            ItemRegistry.registerRenderer(item, withMeta);
        
        return item;
    }
    
    @SideOnly(Side.CLIENT)
    private static boolean registerRenderer(Item item, boolean withMeta) {
        boolean flag = true;
        
        RenderUtil.renderItem(item, 0);
        
        return flag;
    }
}