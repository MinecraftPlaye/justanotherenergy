/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.lib;

import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class ModRecipes {
    
    public static void init() {
    	/*
    	 * Shapeless Recipe
    	 */
    	// pre-industrial age
    	GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(ModItems.plate_copper), ModItems.sledgehammer, "ingotCopper"));
    	GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(ModItems.plate_tin), ModItems.sledgehammer, "ingotTin"));
    	GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(ModBlocks.cable, 4), ModItems.snip, "plateCopper"));
        
        /*
         * Recipe
         */
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.sledgehammer), 
                "III", 
                "ISI", 
                "#I#", 'I', "ingotIron", 'S', "stickWood"));
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.snip), 
                "T#T", 
                "#T#", 
                "S#S", 'T', "plateTin", 'S', "stickWood"));
        GameRegistry.addRecipe(new ItemStack(ModBlocks.cableConnect, 2), 
                "XIX", 'X', Blocks.HARDENED_CLAY, 'I', ModBlocks.cable);
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.capacitor), 
                "#XT", 
                "XPX", 
                "TX#", 'X', "paneGlass", 'P', "paper", 'T', "plateTin"));
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModBlocks.capacitorBank), 
                "ICI", 
                "CXC", 
                "III", 'C', ModItems.capacitor, 'X', ModBlocks.cable, 'I', "ingotIron"));
        
        /*
         * Smelting
         */
        GameRegistry.addSmelting(ModBlocks.ore_copper, new ItemStack(ModItems.ingot_copper), .4f);
        GameRegistry.addSmelting(ModBlocks.ore_tin, new ItemStack(ModItems.ingot_tin), .4f);
    }
}