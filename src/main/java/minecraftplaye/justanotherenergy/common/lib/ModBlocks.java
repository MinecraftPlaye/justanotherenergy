/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.lib;

import minecraftplaye.justanotherenergy.common.blocks.*;
import minecraftplaye.justanotherenergy.common.tileentities.TileEntityCapacitorBank;
import minecraftplaye.justanotherenergy.common.tileentities.TileEntityChunkLoader;
import minecraftplaye.justanotherenergy.common.tileentities.TileEntityCable;
import minecraftplaye.justanotherenergy.common.tileentities.TileEntityCableConnector;
import minecraftplaye.justanotherenergy.common.tileentities.TileEntitySolarPanel;

import net.minecraft.block.Block;

import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModBlocks {
	
	/** Ores */
	public static Block ore_copper;
	public static Block ore_tin;
	
	public static Block cable;
	public static Block cableConnect;
	
	/** Machinery */
	public static Block solarPanel;
	public static Block capacitorBank;
	
	public static Block chunkLoader;
	
	public static void init() {
	    ModBlocks.ore_copper = BlockRegistry.register(new BasicOre("oreCopper"), "ore_copper");
	    ModBlocks.ore_tin = BlockRegistry.register(new BasicOre("oreTin"), "ore_tin");
	    
	    // Cables & Pipes
	    ModBlocks.cable = BlockRegistry.register(new BlockCable(), "cable", true);
	    ModBlocks.cableConnect = BlockRegistry.register(new BlockCableConnector(), "cable_connector");
	    
	    // Machines (Producers)
	    ModBlocks.solarPanel = BlockRegistry.register(new BlockSolarPanel(), "solar_panel");
	    
	    // Machines (Containers)
	    ModBlocks.capacitorBank = BlockRegistry.register(new BlockCapacitorBank(), "capacitor_bank");
	    
	    // Machines (Consumers)
	    
	    // Optional blocks
	    if(ModConfig.CHUNK_LOADER_ALLOWED) {
	        ModBlocks.chunkLoader = BlockRegistry.register(new BlockChunkLoader(), "chunk_loader");
	        GameRegistry.registerTileEntity(TileEntityChunkLoader.class, ModBlocks.chunkLoader.getRegistryName().toString());
	    }
	    
	    // Tile Entities
	    GameRegistry.registerTileEntity(TileEntityCable.class, ModBlocks.cable.getRegistryName().toString());
	    GameRegistry.registerTileEntity(TileEntityCableConnector.class, ModBlocks.cableConnect.getRegistryName().toString());
	    
	    // Tile Entities (Producers)
        GameRegistry.registerTileEntity(TileEntitySolarPanel.class, ModBlocks.solarPanel.getRegistryName().toString());
        
        // Tile Entities (Containers)
		GameRegistry.registerTileEntity(TileEntityCapacitorBank.class, ModBlocks.capacitorBank.getRegistryName().toString());
		
		// Tile Entities (Consumers)
		
		ModBlocks.registerLvl();
	}
	
	private static void registerLvl() {
		
	}
}