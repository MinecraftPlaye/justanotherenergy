/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.lib;

import java.util.List;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

import net.minecraftforge.common.ForgeChunkManager.PlayerOrderedLoadingCallback;
import net.minecraftforge.common.ForgeChunkManager.Ticket;

public class ChunkManagerCallback implements PlayerOrderedLoadingCallback {
	
	@Override
	public void ticketsLoaded(List<Ticket> tickets, World world) {
		for(Ticket ticket : tickets) {
			final NBTTagCompound modData = ticket.getModData();
			if(!modData.hasKey("blockPosition") || !modData.hasKey("size"))
				continue;
			
			ChunkLoaderManager.startChunkLoading(world, ticket);
		}
	}
	
	@Override
	public ListMultimap<String, Ticket> playerTicketsLoaded(ListMultimap<String, Ticket> tickets, World world) {
		final ListMultimap<String, Ticket> copyTickets = ArrayListMultimap.create();
		
		for(String player : tickets.keySet()) {
			for(Ticket ticket : tickets.values()) {
				final NBTTagCompound modData = ticket.getModData();
				if(modData.hasKey("blockPosition") && modData.hasKey("size"))
					copyTickets.put(player, ticket);
			}
		}
		
		return copyTickets;
	}
}