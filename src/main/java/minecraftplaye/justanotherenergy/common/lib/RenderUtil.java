/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.lib;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;

import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Here you can register all your items and blocks
 * which has to be rendered with a texture in-game.
 * 
 * @author MinecraftPlaye
 */
@SideOnly(Side.CLIENT)
public class RenderUtil {
    
    /**
     * Registers the item renderer.
     * This method has to be called in the <code>FMLInitializationEvent</code> event after the
     * RenderUtil <code>init</code> was called.
     * 
     * @param item The Item to be rendered.
     * @param meta The meta-data of the item.
     */
    @SideOnly(Side.CLIENT)
    public static void renderItem(Item item, int meta) {
        ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(item.getRegistryName(), "inventory"));
    }
	
    /**
     * Registers the item renderer.
     * This method has to be called in the <code>FMLInitializationEvent</code> event after the
     * RenderUtil <code>init</code> was called.
     * 
     * @param path The path to the resources
     * @param item The Item to be rendered.
     * @param name The name of the item.
     * @param meta The meta-data of the item.
     */
    @SideOnly(Side.CLIENT)
    public static void renderItem(String path, Item item, String name, int meta) {
        ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(Constants.MOD_ID + ":" + path + name, "inventory"));
    }
    
	/**
	 * Registers the block renderer.
	 * This method has to be called in the <code>FMLInitializationEvent</code> event after the
	 * RenderUtil <code>init</code> was called.
	 * 
	 * @param block The Block to be rendered.
	 * @param meta The meta-data of the block.
	 */
    @SideOnly(Side.CLIENT)
	public static void renderBlock(Block block, int meta) {
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(block), meta, new ModelResourceLocation(block.getRegistryName(), "inventory"));
	}
	
	/**
	 * Registers the block renderer.
	 * This method has to be called in the <code>FMLInitializationEvent</code> event after the
	 * RenderUtil <code>init</code> was called.
	 * 
	 * @param path The path to the resources
	 * @param block The Block to be rendered.
	 * @param name The name of the block
	 * @param meta The meta-data of the block.
	 */
    @SideOnly(Side.CLIENT)
	public static void renderBlock(String path, Block block, String name, int meta) {
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(block), meta, new ModelResourceLocation(Constants.MOD_ID + ":" + path + name, "inventory"));
	}
	
    @SideOnly(Side.CLIENT)
	public static void renderBlock(String path, Block block, int meta, String variant) {
	    ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(block), meta, new ModelResourceLocation(Constants.MOD_ID + ":" + path + variant, "inventory"));
	}
}