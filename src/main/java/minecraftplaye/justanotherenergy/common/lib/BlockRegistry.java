/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.lib;

import java.util.Iterator;
import java.util.function.Function;

import javax.annotation.Nullable;

import minecraftplaye.justanotherenergy.JustAnotherEnergy;
import minecraftplaye.justanotherenergy.api.IItemOreDict;
import minecraftplaye.justanotherenergy.api.IType;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemBlock;

import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * The <code>BlockRegistry</code> class contains several useful class 
 * methods to register Blocks. It cannot be instantiated.
 * 
 * @author MinecraftPlaye
 */
public class BlockRegistry {
	
	/**
	 * Provides the functionality of automatically registering the block and
	 * setting the registry name and unlocalized name. It also registered
	 * the block JSON renderer's and the item block with it's JSON renderer's.
	 * 
	 * @param block The block to register
	 * @param name The unique name of the block
	 * @return The block
	 */
	public static Block register(Block block, String name) {
		return BlockRegistry.register(block, name, ItemBlock::new, true);
	}
	
	/**
	 * Provides the functionality of automatically registering the block and
	 * setting the registry name and unlocalized name. It also registered
	 * the block JSON renderer's and the item block with it's JSON renderer's.
	 * 
	 * @param block The block to register
	 * @param name The unique name of the block
	 * @param withMeta Should the meta data of the block also be registered?
	 * @return The block
	 */
	public static Block register(Block block, String name, boolean withMeta) {
		return BlockRegistry.register(block, name, ItemBlock::new, withMeta);
	}
	
	/**
	 * Provides the functionality of automatically registering the block and
	 * setting the registry name and unlocalized name. It also registered
	 * the block JSON renderer's and the item block with it's JSON renderer's.
	 * 
	 * @param block The block to register
	 * @param name The unique name of the block
	 * @param itemFactory  A function that creates the ItemBlock instance, or null if no ItemBlock should be created
	 * @return The block
	 */
	public static Block register(Block block, String name, @Nullable Function<Block, ItemBlock> itemFactory) {
		return BlockRegistry.register(block, name, itemFactory, true);
	}
	
	/**
	 * Provides the functionality of automatically registering the block and
	 * setting the registry name and unlocalized name. It also registered
	 * the block JSON renderer's and the item block with it's JSON renderer's.
	 * 
	 * @param block The block to register
	 * @param name The unique name of the block
	 * @param itemFactory  A function that creates the ItemBlock instance, or null if no ItemBlock should be created
	 * @param withMeta Should the meta data of the block also be registered?
	 * @return The block
	 */
	public static Block register(Block block, String name, @Nullable Function<Block, ItemBlock> itemFactory, boolean withMeta) {
		block.setUnlocalizedName(Constants.MOD_ID.toLowerCase() + "." + name);
		block.setRegistryName(name);
		GameRegistry.register(block);
		
		if(itemFactory != null) {
			final ItemBlock itemBlock = itemFactory.apply(block);
			GameRegistry.register(itemBlock.setRegistryName(block.getRegistryName()));
			
			if(block instanceof IItemOreDict)
				((IItemOreDict)block).initOreDict();
			
			if(itemBlock instanceof IItemOreDict)
				((IItemOreDict)itemBlock).initOreDict();
			
			if(FMLCommonHandler.instance().getEffectiveSide().isClient())
				BlockRegistry.registerRenderer(block, withMeta);
		}
		
		return block;
	}
	
	/**
	 * Provides the functionality of automatically registering the block 
	 * JSON renderer's and the item block with it's JSON renderer's.
	 * 
	 * @param block The block to register
	 * @param withMeta Should the meta data of the block also be registered?
	 * @return If the registration of the JSON renderer was successful.
	 */
	@SideOnly(Side.CLIENT)
	private static boolean registerRenderer(Block block, boolean withMeta) {
		Iterator<IBlockState> it = block.getBlockState().getValidStates().iterator();
		boolean flag = true;
		
		while(it.hasNext()) {
			IBlockState state = it.next();
			
			if(state == null) {
				JustAnotherEnergy.getLogger().error("Skipping block rendering registration for {}", state);
				flag = false;
				break;
			}
			
			int meta = block.getMetaFromState(state);
			RenderUtil.renderBlock(block, withMeta ? meta : 0);
		}
		return flag;
	}
	
	/**
	 * Provides the functionality of automatically registering the block and
	 * setting the registry name and unlocalized name. It also registered
	 * the block JSON renderer's and the item block with it's JSON renderer's.
	 * <p>
	 * This method tells Minecraft that the model json's can be found in a sub-folder.
	 * 
	 * @param block The block to register
	 * @param name The unique name of the block
	 * @param itemFactory  A function that creates the ItemBlock instance, or null if no ItemBlock should be created
	 * @param variants The different variants in which block comes
	 * @return The block
	 */
	public static <T extends IType> Block register(Block block, String name, @Nullable Function<Block, ItemBlock> itemFactory, @Nullable T[] variants) {
		return BlockRegistry.register(block, name, itemFactory, true, variants);
	}
	
	/**
	 * Provides the functionality of automatically registering the block and
	 * setting the registry name and unlocalized name. It also registered
	 * the block JSON renderer's and the item block with it's JSON renderer's.
	 * <p>
	 * This method tells Minecraft that the model json's can be found in a sub-folder.
	 * 
	 * @param block The block to register
	 * @param name The unique name of the block
	 * @param itemFactory  A function that creates the ItemBlock instance, or null if no ItemBlock should be created
	 * @param withMeta Should the meta data of the block also be registered?
	 * @param variants The different variants in which block comes
	 * @return The block
	 */
	public static <T extends IType> Block register(Block block, String name, @Nullable Function<Block, ItemBlock> itemFactory, boolean withMeta, @Nullable T[] variants) {
		block.setUnlocalizedName(Constants.MOD_ID.toLowerCase() + "." + name);
		block.setRegistryName(name);
		GameRegistry.register(block);
		
		if(itemFactory != null) {
			final ItemBlock itemBlock = itemFactory.apply(block);
			GameRegistry.register(itemBlock.setRegistryName(block.getRegistryName()));
			
			if(FMLCommonHandler.instance().getEffectiveSide().isClient()) {
				Iterator<IBlockState> it = block.getBlockState().getValidStates().iterator();
				
				while(it.hasNext()) {
					IBlockState state = it.next();
					
					if(state == null) {
						JustAnotherEnergy.getLogger().error("Skipping block rendering registration for {}", state);
						break;
					}
					
					for(T variant : variants) {
						RenderUtil.renderBlock(BlockRegistry.getResourcePath(name), block, variant.getID(), variant.getName());
					}
				}
			}
		}
		
		return block;
	}
	
    /**
     * Returns the path sub-folder in which the JSON file can be found, as
     * a String. <p>
     * If there is no sub-folder, it will return an empty string.
     * 
     * @param name The name of the block
     * @return The sub-folder as a string
     */
    private static String getResourcePath(String name) {
        String path = "";
        switch(name) {
    		case "stone": path = "stone/"; break;
    		case "stone_brick": path = "stoneBrick/"; break;
    		case "log_nature": path = "wood/logs/"; break;
    		case "log_horizontal": path = "wood/logs/"; break; 
    		case "log_vertical": path = "wood/logs/"; break;
    		case "wood_planks": path = "wood/planks/"; break;
    		default: path = ""; break;
        }
        return path;
    }
}