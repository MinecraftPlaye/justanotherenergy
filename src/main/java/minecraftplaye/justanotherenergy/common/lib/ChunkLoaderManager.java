/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.lib;

import minecraftplaye.justanotherenergy.common.tileentities.TileEntityChunkLoader;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.ForgeChunkManager.Ticket;

public class ChunkLoaderManager {
	
	public static void startChunkLoading(World world, Ticket ticket) {
		if(world == null || ticket == null)
			return;
		
		NBTTagCompound modData = ticket.getModData();
		if(!modData.hasKey("blockPosition") || !modData.hasKey("size"))
			return;
		
		BlockPos pos = NBTUtil.getPosFromTag(modData.getCompoundTag("blockPosition"));
		TileEntity tile = world.getTileEntity(pos);
		if(!(tile instanceof TileEntityChunkLoader))
			return;
		
		int size = modData.getInteger("size");
		
		if(size == 1)
			ForgeChunkManager.forceChunk(ticket, new ChunkPos(pos));
		
		// TODO: load radius, eg. size 9 = 9 chunks loaded
		
		((TileEntityChunkLoader) tile).setTicket(ticket);
	}
	
	public static void stopChunkLoading(World world, Ticket ticket) {
		if(world == null || ticket == null)
			return;
		
		NBTTagCompound modData = ticket.getModData();
		if(!modData.hasKey("blockPosition") || !modData.hasKey("size"))
			return;
		
		int size = modData.getInteger("size");
		BlockPos pos = NBTUtil.getPosFromTag(modData.getCompoundTag("blockPosition"));
		
		if(size == 1)
			ForgeChunkManager.unforceChunk(ticket, new ChunkPos(pos));
	}
}
