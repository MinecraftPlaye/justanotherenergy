/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.lib;

import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Config(modid = Constants.MOD_ID)
@Config.LangKey("config.justanotherenergy.title")
public class ModConfig {
    
    @Config.LangKey("config.justanotherenergy.general.energyname")
    @Config.Comment("The name of the energy to be displayed. "
            + "For example FU (Forge Units), FE (Forge Energy), T (Tesla) or RF (Redstone Flux). "
            + "WARNING: This doesn't effect the used energy system!")
    public static String ENERGY_NAME = "FU";
    
    @Config.LangKey("config.justanotherenergy.general.oregen")
    @Config.Comment("Should ores be generated by this mod?")
    public static boolean ENABLE_ORE_GEN = true;
    
    @Config.LangKey("config.justanotherenergy.chunkloader.allowed")
    @Config.Comment("Should the chunk loader be registered? Requires Restart!")
    public static boolean CHUNK_LOADER_ALLOWED = false;
    
    @Mod.EventBusSubscriber
    private static class ConfigHandler {
        
        /**
         * Inject the new values and save to the config file when the
         * config has been changed from the GUI.
         * 
         * @param event the event
         */
        @SubscribeEvent
        public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
            if(event.getModID().equals(Constants.MOD_ID))
                ConfigManager.load(Constants.MOD_ID, Config.Type.INSTANCE);
        }
    }
}