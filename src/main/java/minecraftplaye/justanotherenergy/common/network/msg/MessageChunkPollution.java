/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.network.msg;

import io.netty.buffer.ByteBuf;

import minecraftplaye.justanotherenergy.api.pollution.IChunkPollution;
import minecraftplaye.justanotherenergy.api.pollution.IChunkPollutionHolder;
import minecraftplaye.justanotherenergy.common.power.pollution.CapabilityPollution;
import net.minecraft.client.Minecraft;
import net.minecraft.util.IThreadListener;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Sent from the server to update the pollution value of the chunk.
 * 
 * @author MinecraftPlaye
 */
public class MessageChunkPollution implements IMessage {
    
    /**
     * The x position of the chunk.
     */
    private int chunkPosX;
    
    /**
     * The z position of the chunk.
     */
    private int chunkPosZ;
    
    /**
     * The pollution value of the chunk.
     */
    private double pollution = 0;
    
    /** Default constructor, as it is required. */
    public MessageChunkPollution() { }
    
    public MessageChunkPollution(ChunkPos pos, IChunkPollution pollution) {
        this.chunkPosX = pos.x;
        this.chunkPosZ = pos.z;
        this.pollution = pollution.getPollution();
    }
    
    @Override
    public void fromBytes(ByteBuf buf) {
        this.chunkPosX = buf.readInt();
        this.chunkPosZ = buf.readInt();
        this.pollution = buf.readDouble();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.chunkPosX);
        buf.writeInt(this.chunkPosX);
        buf.writeDouble(this.pollution);
    }
    
    /**
     * This message handler is used to let the Client
     * change the amount of pollution.
     * 
     * @author MinecraftPlaye
     */
    public static class MessageChunkPollutionHandler implements IMessageHandler<MessageChunkPollution, IMessage> {
        
        @Override
        public IMessage onMessage(MessageChunkPollution message, MessageContext ctx) {
            IThreadListener mainThread = Minecraft.getMinecraft();
            mainThread.addScheduledTask(() -> {
                final World world = Minecraft.getMinecraft().world;
                
                final IChunkPollutionHolder chunkPollutionHolder = CapabilityPollution.getChunkPollutionHolder(world);
                if(chunkPollutionHolder == null) return;
                
                final IChunkPollution chunkPollution = chunkPollutionHolder.getChunkPollution(new ChunkPos(message.chunkPosX, message.chunkPosZ));
                if(chunkPollution == null) return;
                
                chunkPollution.setPollution(message.pollution);
            });
            
            // No respond packet
            return null;
        }
    }
}