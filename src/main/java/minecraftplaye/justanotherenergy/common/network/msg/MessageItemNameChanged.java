/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.network.msg;

import io.netty.buffer.ByteBuf;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IThreadListener;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * This message is used to inform the Server that
 * the name of the Lexicon should be changed.
 * 
 * @author MinecraftPlaye
 */
public class MessageItemNameChanged implements IMessage {
    
    private String name;
    
    /** Default constructor, as it is required. */
    public MessageItemNameChanged() { }
    
    /**
     * A Message to rename the lexicon item
     * resulting into the new <code>name</code>.
     * 
     * @param name the name to change into
     */
    public MessageItemNameChanged(String name) {
        this.name = name;
    }
    
    @Override
    public void fromBytes(ByteBuf buf) {
        this.name = ByteBufUtils.readUTF8String(buf);
    }
    
    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.name);
    }
    
    /**
     * This message handler is used to let the Server
     * change the name of the Lexicon.
     * 
     * @author MinecraftPlaye
     */
    public static class MessageItemNameChangedHandler implements IMessageHandler<MessageItemNameChanged, IMessage> {
        
        /** Default constructor, as it is required. */
        public MessageItemNameChangedHandler() { }
        
        @Override
        public IMessage onMessage(MessageItemNameChanged message, MessageContext ctx) {
            IThreadListener mainThread = (WorldServer) ctx.getServerHandler().player.getEntityWorld();
            mainThread.addScheduledTask(() -> {
                // This is the player the packet was sent to the server from
                EntityPlayerMP serverPlayer = ctx.getServerHandler().player;
                
                ItemStack stack = serverPlayer.getHeldItemMainhand();
                stack.setStackDisplayName(TextFormatting.GOLD + "" + TextFormatting.BOLD + "" + message.name);
            });
            
            // No respond packet
            return null;
        }
    }
}