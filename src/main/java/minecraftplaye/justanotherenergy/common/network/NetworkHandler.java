/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.network;

import minecraftplaye.justanotherenergy.JustAnotherEnergy;
import minecraftplaye.justanotherenergy.common.lib.Constants;
import minecraftplaye.justanotherenergy.common.network.msg.*;
import minecraftplaye.justanotherenergy.common.network.msg.MessageChunkPollution.MessageChunkPollutionHandler;
import minecraftplaye.justanotherenergy.common.network.msg.MessageItemNameChanged.MessageItemNameChangedHandler;

import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

/**
 * This network handler registers all packets and the
 * GUI handler for this modification.
 * 
 * @author MinecraftPlaye
 */
public class NetworkHandler {
    
    /** The id of the packet. */
    private static int packetID = 0;
    /** The instance of the network handler. */
    public static final SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel(Constants.MOD_ID);
    
    /**
     * Initializes and registers the packets and the GUI handler.
     */
    public static void init() {
        NetworkRegistry.INSTANCE.registerGuiHandler(JustAnotherEnergy.getInstance(), JustAnotherEnergy.handler);
        
        // fourth parameter = the side that your packet will be received on
        
        // Server Messages (from Client to Server)
        NetworkHandler.INSTANCE.registerMessage(MessageItemNameChangedHandler.class, MessageItemNameChanged.class, NetworkHandler.getPacketID(), Side.SERVER);
        
        // Client Messages (from Server to Client)
        NetworkHandler.INSTANCE.registerMessage(MessageChunkPollutionHandler.class, MessageChunkPollution.class, NetworkHandler.getPacketID(), Side.CLIENT);
    }
    
    /**
     * Increases the packet id to ensure no
     * packet has the same id as an already
     * registered packet.
     * 
     * @return the new packet id
     */
    private static int getPacketID() {
        return packetID++;
    }
}