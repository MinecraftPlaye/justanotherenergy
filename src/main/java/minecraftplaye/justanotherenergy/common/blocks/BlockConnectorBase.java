/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.blocks;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

/**
 * This class defines the basic behaviour of
 * blocks which connect to each other within this mod.
 * 
 * @author MinecraftPlaye
 */
public abstract class BlockConnectorBase extends BasicBlock {
    
    /**
     * The minimum bounding box for this block.
     */
    public static float MIN_BB = .25f;
    
    /**
     * The maximum bounding box for this block.
     */
    public static float MAX_BB = .75f;
    
    // JSON properties
    public static final ImmutableList<IProperty<Boolean>> CONNECTED_PROPERTIES = ImmutableList.copyOf(
            Stream.of(EnumFacing.VALUES)
                .map(facing -> PropertyBool.create(facing.getName()))
                .collect(Collectors.toList())
            );
    
    // Collision Boxes
    public static final ImmutableList<AxisAlignedBB> CONNECTED_BOUNDING_BOXES  = ImmutableList.copyOf(
            Stream.of(EnumFacing.VALUES)
                .map(facing -> {
                    Vec3i direction = facing.getDirectionVec();
                    // add new auto generated BB
                    return new AxisAlignedBB(
                            getMinBB(direction.getX()), getMinBB(direction.getY()), getMinBB(direction.getZ()),
                            getMaxBB(direction.getX()), getMaxBB(direction.getY()), getMaxBB(direction.getZ())
                            );
                })
                .collect(Collectors.toList())
            );
    
    public BlockConnectorBase(float hardness, float resistance) {
        super(Material.IRON, SoundType.METAL, hardness, resistance);
    }
    
    /**
     * Calculates the bounding box of the block.
     * If the passed value {@code direction} is -1
     * this function will return 0 (the minimum).
     * 
     * @param direction the direction the block is facing
     * @return the minimum bounding box
     */
    private static float getMinBB(int direction) {
        return direction == -1 ? 0 : MIN_BB;
    }
    
    /**
     * Calculates the bounding box of the block.
     * If the passed value {@code direction} is 1
     * this function will return 1 (the maximum).
     * 
     * @param direction the direction the block is facing
     * @return the maximum bounding box
     */
    private static float getMaxBB(int direction) {
        return direction == 1 ? 1 : MAX_BB;
    }
    
    /**
     * Is the neighbour block a valid connection for this connector?
     * As default it will return true when the neighbour block is
     * of this block type.
     * 
     * @param worldIn The world
     * @param pos The connector's position
     * @param neighbourState The neighbour block's state
     * @param facing The direction of the neighbour block
     * @return Is the neighbour block a valid connection?
     */
    protected boolean isValidConnection(final IBlockAccess worldIn, final BlockPos pos, final IBlockState neighbourState, EnumFacing facing) {
        return neighbourState.getBlock() instanceof BlockConnectorBase;
    }
    
    /**
     * Can this pipe connect to the neighbour block?
     * 
     * @param worldIn The world
     * @param state The connector's state
     * @param pos The connector's position
     * @param facing The direction of the neighbour block
     * @return Can this pipe connect?
     */
    private boolean canConnectTo(final IBlockAccess worldIn, final IBlockState state, final BlockPos pos, EnumFacing facing) {
        final BlockPos neighborPos = pos.offset(facing);
        final IBlockState neighbourState = worldIn.getBlockState(neighborPos);
        
        // check if THIS block can be connected to the neighbour block
        final boolean isNeighborValid = this.isValidConnection(worldIn, pos, neighbourState, facing);
        // check if the neighbour block can be connected to this
        // when the neighbour block is not of this type, it might be a machine, meaning that isValidConnection isn't available
        final boolean isThisValid = !(neighbourState instanceof BlockConnectorBase) || ((BlockConnectorBase) neighbourState.getBlock()).isValidConnection(worldIn, pos, neighbourState, facing);
        
        return isNeighborValid && isThisValid;
    }
    
    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        for(final EnumFacing facing : EnumFacing.VALUES)
            state = state.withProperty(CONNECTED_PROPERTIES.get(facing.getIndex()), this.canConnectTo(worldIn, state, pos, facing));
        
        return state;
    }
    
    @Override
    public void addCollisionBoxToList(IBlockState state, World worldIn, BlockPos pos, AxisAlignedBB entityBox, 
            List<AxisAlignedBB> collidingBoxes, Entity entityIn, boolean p_185477_7_) {
        final AxisAlignedBB BOX_CENTER = new AxisAlignedBB(.25f, .25f, .25f, .75f, .75f, .75f);
        Block.addCollisionBoxToList(pos, entityBox, collidingBoxes, BOX_CENTER);
        
        // without this check, weird stuff will occur
        state = this.getActualState(state, worldIn, pos);
        
        for(final EnumFacing facing : EnumFacing.VALUES) {
            // check if we are connected
            if(state.getValue(CONNECTED_PROPERTIES.get(facing.getIndex()))) {
                final AxisAlignedBB bb = CONNECTED_BOUNDING_BOXES.get(facing.getIndex());
                Block.addCollisionBoxToList(pos, entityBox, collidingBoxes, bb);
            }
        }
    }
    
    // TODO: calculate bounding box automatically... or otherwise write 64 different states manually :P
    /*@Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        state = this.getActualState(state, source, pos);
        return CONNECTED_BOUNDING_BOXES.get(this.getBoundingBoxIndex(state));
    }
    
    private int getBoundingBoxIndex(IBlockState state) {
        int index = 0;
        
        System.out.println(CONNECTED_PROPERTIES.size());
        for(int i = 0; i < CONNECTED_PROPERTIES.size(); i++) {
            PropertyBool value = (PropertyBool) CONNECTED_PROPERTIES.get(i);
            
            if((Boolean)state.getValue(value).booleanValue())
                index |= 1 << EnumFacing.getFront(i).getIndex();
            System.out.println(i + " " + index);
        }
        
        ArrayList<AxisAlignedBB> bb = new ArrayList<>();
        bb.add(FULL_BLOCK_AABB);
        
        return index;
    }*/
    
    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, CONNECTED_PROPERTIES.toArray(new IProperty[CONNECTED_PROPERTIES.size()]));
    }
    
    @Override
    public int getMetaFromState(IBlockState state) {
        return 0;
    }
    
    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState();
    }
    
    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }
    
    @Override
    public boolean isFullBlock(IBlockState state) {
        return false;
    }
    
    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }
}