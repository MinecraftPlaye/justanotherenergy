/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.blocks;

import minecraftplaye.justanotherenergy.common.lib.ModCreativeTabs;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BasicBlock extends Block {
    
    public BasicBlock(Material materialIn, SoundType sound, float hardness, float resistance) {
        super(materialIn);
        
        this.setSoundType(sound);
        this.setHardness(hardness);
        this.setResistance(resistance);
        
        this.setCreativeTab(ModCreativeTabs.mcpowerTab);
    }
}
