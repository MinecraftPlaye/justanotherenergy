/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.blocks;

import java.util.Random;

import minecraftplaye.justanotherenergy.GuiHandler;
import minecraftplaye.justanotherenergy.JustAnotherEnergy;
import minecraftplaye.justanotherenergy.api.pollution.IChunkPollution;
import minecraftplaye.justanotherenergy.common.power.pollution.CapabilityPollution;
import minecraftplaye.justanotherenergy.common.tileentities.TileEntitySolarPanel;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;

public class BlockSolarPanel extends BasicBlock {
	
	public BlockSolarPanel() {
		super(Material.IRON, SoundType.METAL, 3.f, 15.f);
		this.isBlockContainer = true;
		this.setLightOpacity(0);
		this.setTickRandomly(true);
	}
	
	@Override
	public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state) {
	    super.onBlockAdded(worldIn, pos, state);
	    
	    IChunkPollution pol = CapabilityPollution.getChunkPollutionHolder(worldIn).getChunkPollution(new ChunkPos(pos));
	    pol.polluteChunk(150.59d);
	}
	
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
	    super.breakBlock(worldIn, pos, state);
	    
	    IChunkPollution pol = CapabilityPollution.getChunkPollutionHolder(worldIn).getChunkPollution(new ChunkPos(pos));
        pol.decreasePollution(150.59d);
	}
	
	@Override
	public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand) {
	    super.updateTick(worldIn, pos, state, rand);
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn,
	        EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if(worldIn.isRemote)
            return false;
        
        TileEntity tileEntity = worldIn.getTileEntity(pos);
        if(tileEntity instanceof TileEntitySolarPanel) {
            playerIn.openGui(JustAnotherEnergy.getInstance(), GuiHandler.GUI_ID_SOLAR_PANEL, worldIn, pos.getX(), pos.getY(), pos.getZ());
            return true;
        }
        
        return super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
	}
	
	@Override
	public boolean hasTileEntity(IBlockState state) {
	    return true;
	}
	
	@Override
	public TileEntity createTileEntity(World world, IBlockState state) {
	    return new TileEntitySolarPanel();
	}
}