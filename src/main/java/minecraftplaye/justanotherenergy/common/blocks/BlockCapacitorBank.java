/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.blocks;

import minecraftplaye.justanotherenergy.common.tileentities.TileEntityCapacitorBank;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockCapacitorBank extends BasicBlock {
    
    public BlockCapacitorBank() {
        super(Material.IRON, SoundType.METAL, 3.f, 15.f);
    }
    
    @Override
    public boolean hasTileEntity(IBlockState state) {
        return true;
    }
    
    @Override
    public TileEntity createTileEntity(World world, IBlockState state) {
        return new TileEntityCapacitorBank();
    }
}