/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.blocks;

import minecraftplaye.justanotherenergy.api.IItemOreDict;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

import net.minecraftforge.oredict.OreDictionary;

public class BasicOre extends BasicBlock implements IItemOreDict{
    
    private String ore_dictionary;
    
    public BasicOre(String ore_dictionary) {
        this(ore_dictionary, 3.f, 5.f);
    }
    
    public BasicOre(String ore_dictionary, float hardness, float resistance) {
        super(Material.ROCK, SoundType.STONE, 3.f, 5.f);
        this.ore_dictionary = ore_dictionary;
    }

    @Override
    public void initOreDict() {
        OreDictionary.registerOre(this.ore_dictionary, this);
    }
}