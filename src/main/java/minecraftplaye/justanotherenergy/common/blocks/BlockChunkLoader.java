/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.blocks;

import minecraftplaye.justanotherenergy.JustAnotherEnergy;
import minecraftplaye.justanotherenergy.api.ChunkLoaderTypes;
import minecraftplaye.justanotherenergy.common.tileentities.TileEntityChunkLoader;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.ForgeChunkManager.Type;

public class BlockChunkLoader extends BasicBlock {
    
    public static PropertyEnum<ChunkLoaderTypes> META_PROPERTY = PropertyEnum.create("loader", ChunkLoaderTypes.class);
    
    public BlockChunkLoader() {
        super(Material.IRON, SoundType.METAL, 3.f, 15.f);
        this.setDefaultState(this.blockState.getBaseState().withProperty(META_PROPERTY, ChunkLoaderTypes.LOAD_01));
    }
    
    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
        if(worldIn.isRemote)
            return;
        
        TileEntity tile = worldIn.getTileEntity(pos);
        if(tile != null && tile instanceof TileEntityChunkLoader) {
            TileEntityChunkLoader tileCL = (TileEntityChunkLoader) tile;
            
            if(placer instanceof EntityPlayer) {
                EntityPlayer player = (EntityPlayer) placer;
                tileCL.setOwnerId(player.getUniqueID());
                
                final ForgeChunkManager.Ticket ticket = ForgeChunkManager.requestPlayerTicket(JustAnotherEnergy.getInstance(), tileCL.getOwnerIdString(), worldIn, Type.NORMAL);
                
                if(ticket == null) {
                	// Forge will log errors here, too
                	JustAnotherEnergy.getLogger().warn("Chunkloading at {} failed. Most likely the limit was reached. {}", pos, ForgeChunkManager.ticketCountAvailableFor(player.getName()));
                	return;
                }
                
                final NBTTagCompound modData = ticket.getModData();
                modData.setTag("blockPosition", NBTUtil.createPosTag(pos));
                
                if(this.getMetaFromState(state) == ChunkLoaderTypes.LOAD_01.getID())
                    modData.setInteger("size", 1);
                else if(this.getMetaFromState(state) == ChunkLoaderTypes.LOAD_09.getID())
                	modData.setInteger("size", 9);
                else if(this.getMetaFromState(state) == ChunkLoaderTypes.LOAD_25.getID())
                	modData.setInteger("size", 25);
                
                tileCL.setTicket(ticket);
                //tileCL.setDefaultTicket(ticket);
            }
        }
    }
    
    @Override
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
        TileEntity tile = worldIn.getTileEntity(pos);
        if(tile != null && tile instanceof TileEntityChunkLoader) {
            TileEntityChunkLoader tileCL = (TileEntityChunkLoader) tile;
            ForgeChunkManager.releaseTicket(tileCL.getTicket());
        }
        
        super.breakBlock(worldIn, pos, state);
    }
    
    @Override
    public boolean hasTileEntity(IBlockState state) {
        return true;
    }
    
    @Override
    public TileEntity createTileEntity(World world, IBlockState state) {
        switch(this.getMetaFromState(state)) {
            case 0: return new TileEntityChunkLoader(500, 500, 100); 
            case 1: return new TileEntityChunkLoader(4500, 4500, 900);
            case 2: return new TileEntityChunkLoader(125000, 125000, 25000);
            default: return new TileEntityChunkLoader(500, 500, 100);   
        }
    }
    
    @Override
    public void getSubBlocks(Item itemIn, CreativeTabs tab, NonNullList<ItemStack> list) {
        for(final ChunkLoaderTypes type : ChunkLoaderTypes.values())
            list.add(new ItemStack(this, 1, type.getID()));
    }
    
    @Override
    public int damageDropped(IBlockState state) {
        return this.getMetaFromState(state);
    }
    
    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, META_PROPERTY);
    }
    
    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(META_PROPERTY).getID();
    }
    
    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(META_PROPERTY, ChunkLoaderTypes.byMetadata(meta));
    }
}