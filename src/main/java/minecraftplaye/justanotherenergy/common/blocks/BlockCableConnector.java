/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.blocks;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;

import minecraftplaye.justanotherenergy.common.power.EnergyUtils;
import minecraftplaye.justanotherenergy.common.tileentities.TileEntityEnergy;
import minecraftplaye.justanotherenergy.common.tileentities.TileEntityCableConnector;

import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockCableConnector extends BlockConnectorBase {
    
    // JSON properties
    public static final ImmutableList<IProperty<Boolean>> SPECIAL_PROPERTIES = ImmutableList.copyOf(
            Stream.of(EnumFacing.VALUES)
                .map(facing -> PropertyBool.create("sp_" + facing.getName()))
                .collect(Collectors.toList())
            );
    
    public BlockCableConnector() {
        super(3.f, 15.f);
    }
    
    @Override
    public void onNeighborChange(IBlockAccess world, BlockPos pos, BlockPos neighbor) {
    	super.onNeighborChange(world, pos, neighbor);
    	
    	// World implements IBlockAccess
    	if(((World) world).isRemote)
    	    return;
    	
    	this.neighbourUpdate(world, pos, neighbor);
    }
    
    /**
     * Adds all neighbour cable connectors to the connected list of <b>THIS</b> blocks
     * tile entity.
     * The method should be called when a neighbour block updates or when the connected
     * list is empty.
     * 
     * @param world The world
     * @param pos Block position in world
     * @param neighbor Block position of neighbour
     */
    private void neighbourUpdate(IBlockAccess world, BlockPos pos, BlockPos neighbor) {
        // add direct connections to the tile entity
        TileEntity tile = world.getTileEntity(pos);
        if(tile instanceof TileEntityCableConnector) {
            TileEntityCableConnector pipe = (TileEntityCableConnector) tile;
            
            if(world.getTileEntity(neighbor) != null) {
                // check for all directions
                TileEntity neighbor_te = world.getTileEntity(neighbor);
                if(neighbor_te instanceof TileEntityEnergy)
                    pipe.shouldRecalculate = true;
                else {
                    // check the facing of the block to make sure which side is connected to us
                    for(EnumFacing face : EnumFacing.VALUES) {
                        BlockPos offset = pos.offset(face);
                        if(offset == neighbor) {
                            if(EnergyUtils.hasCapability(neighbor_te, face))
                                pipe.shouldRecalculate = true;
                        }
                    }
                }
            } else {
                if(pipe.getConnections().containsKey(neighbor)) {
                    // remove dead objects from the list
                    pipe.shouldRecalculate = true;
                }
            }
        }
    }
    
    @Override
    protected boolean isValidConnection(IBlockAccess worldIn, BlockPos pos, IBlockState neighbourState, EnumFacing facing) {
        if(super.isValidConnection(worldIn, pos, neighbourState, facing))
            return true;
        
        TileEntity tile = worldIn.getTileEntity(pos.offset(facing));
        if(tile != null)
            return EnergyUtils.hasCapability(tile, facing);
        
        return false;
    }
    
    /**
     * Can this pipe connect to a neighbouring producer machine?
     * 
     * @param worldIn The world
     * @param state The connector's state
     * @param pos The connector's position
     * @param facing The direction of the neighbour block
     * @return Can this pipe connect?
     */
    private boolean canConnectToMachine(final IBlockAccess worldIn, final IBlockState state, final BlockPos pos, EnumFacing facing) {
        final BlockPos neighborPos = pos.offset(facing);
        final IBlockState neighbourState = worldIn.getBlockState(neighborPos);
        
        // check if THIS block can be connected to the neighbour block
        final boolean isNeighborValid = this.isValidConnection(worldIn, pos, neighbourState, facing) && !(neighbourState.getBlock() instanceof BlockConnectorBase);
        final boolean isThisValid = !(neighbourState instanceof BlockCableConnector) && !(neighbourState instanceof BlockCable);
        
        return isNeighborValid && isThisValid;
    }
    
    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        for(final EnumFacing facing : EnumFacing.VALUES)
            state = super.getActualState(state, worldIn, pos).withProperty(SPECIAL_PROPERTIES.get(facing.getIndex()), this.canConnectToMachine(worldIn, state, pos, facing));
        
        return state;
    }
    
    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer.Builder(this)
                .add(CONNECTED_PROPERTIES.toArray(new IProperty[CONNECTED_PROPERTIES.size()]))
                .add(SPECIAL_PROPERTIES.toArray(new IProperty[SPECIAL_PROPERTIES.size()]))
                .build();
    }
    
    @Override
    public boolean hasTileEntity(IBlockState state) {
        return true;
    }
    
    @Override
    public TileEntity createTileEntity(World world, IBlockState state) {
        return new TileEntityCableConnector();
    }
}