/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.blocks;

import minecraftplaye.justanotherenergy.common.power.EnergyUtils;
import minecraftplaye.justanotherenergy.common.tileentities.TileEntityCable;

import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockCable extends BlockConnectorBase {
    
	public BlockCable() {
		super(3.f, 15.f);
	}
	
	@Override
	protected boolean isValidConnection(IBlockAccess worldIn, BlockPos pos, IBlockState neighbourState, EnumFacing facing) {
	    if(super.isValidConnection(worldIn, pos, neighbourState, facing))
	        return true;
	    
	    TileEntity tile = worldIn.getTileEntity(pos.offset(facing));
	    if(tile != null)
	        return EnergyUtils.hasCapability(tile, facing);
	    
	    return false;
	}
	
	@Override
	public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state) {
	    super.onBlockAdded(worldIn, pos, state);
	    
	    TileEntity tile = worldIn.getTileEntity(pos);
	    if(tile != null && tile instanceof TileEntityCable) {
	        TileEntityCable cable = (TileEntityCable) tile;
	        cable.searchNetwork();
	    }
	}
	
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
       TileEntity tile = worldIn.getTileEntity(pos);
        if(tile != null && tile instanceof TileEntityCable) {
            TileEntityCable cable = (TileEntityCable) tile;
            cable.searchNetwork();
        }
        
        super.breakBlock(worldIn, pos, state);
	}
	
	@Override
	public boolean isSideSolid(IBlockState base_state, IBlockAccess world, BlockPos pos, EnumFacing side) {
		return super.isSideSolid(base_state, world, pos, side);
	}
	
	@Override
	public boolean canBeReplacedByLeaves(IBlockState state, IBlockAccess world, BlockPos pos) {
		return false;
	}
	
    @Override
    public boolean hasTileEntity(IBlockState state) {
        return true;
    }
	
	@Override
	public TileEntity createTileEntity(World world, IBlockState state) {
	    return new TileEntityCable();
	}
}