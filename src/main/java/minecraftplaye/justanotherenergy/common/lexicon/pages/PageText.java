/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.lexicon.pages;

import java.util.ArrayList;

import minecraftplaye.justanotherenergy.api.lexicon.IChapterGui;
import minecraftplaye.justanotherenergy.api.lexicon.LexiconPage;
import minecraftplaye.justanotherenergy.client.renderer.RenderHelper;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.resources.I18n;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PageText extends LexiconPage {
    
    public PageText(int pageNumber) {
        super(pageNumber);
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void initGui(IChapterGui gui) {
       // Nothing to do here 
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void renderScreen(IChapterGui gui, int mouseX, int mouseY) {
        int posX = gui.getLeft() + 20;
        int posY = gui.getTop() + 10;
        int color = 0;
        
        FontRenderer font = RenderHelper.getFontRenderer();
        boolean tmpUnicode = font.getUnicodeFlag();
        font.setUnicodeFlag(true);
        
        String text = I18n.format(this.getUnlocalizedContent()).replaceAll("§", "\u00A7");
        String[] data = text.split("<br>");
        
        ArrayList<String> lines = new ArrayList<>();
        
        // each paragraph
        for(String s : data) {
            int spaceRequired = 0;
            String complete = "";
            String[] tokens = s.split(" ");
            // each word in paragraph
            for(String word : tokens) {
                spaceRequired += (font.getStringWidth(word) + font.getStringWidth(" "));
                
                if(spaceRequired >= gui.getWidth() - 25) {
                    lines.add(complete);
                    complete = word + " ";
                    spaceRequired = font.getStringWidth(complete);
                } else complete += word + " ";
            }
            lines.add(complete);
            lines.add("");
        }
        
        for(int i = 0; i < lines.size(); i++) {
            font.drawString(lines.get(i), posX, posY, color);
            posY += 10;
        }
        
        font.setUnicodeFlag(tmpUnicode);
    }
}