/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.lexicon;

import minecraftplaye.justanotherenergy.api.JustAnotherEnergyAPI;
import minecraftplaye.justanotherenergy.api.lexicon.LexiconChapter;
import minecraftplaye.justanotherenergy.api.lexicon.LexiconPage;
import minecraftplaye.justanotherenergy.common.lib.Constants;

public class BasicChapter extends LexiconChapter {
    
    private String title;
    
    public BasicChapter(String title) {
        super(Constants.MOD_ID + ".chapter." + title);
        this.title = title;
        JustAnotherEnergyAPI.addChapter(this);
    }
    
    @Override
    public LexiconChapter addAllPages(LexiconPage... pages) {
        for(LexiconPage page : pages)
            page.unlocalizedContent = Constants.MOD_ID + ".page." + this.title + page.getPage();
        
        return super.addAllPages(pages);
    }
}