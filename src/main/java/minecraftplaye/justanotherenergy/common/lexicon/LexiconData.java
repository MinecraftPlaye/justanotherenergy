/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.lexicon;

import minecraftplaye.justanotherenergy.api.lexicon.LexiconChapter;
import minecraftplaye.justanotherenergy.common.lexicon.pages.PageText;

/**
 * This class holds all the data
 * for the lexicon to display.
 * 
 * @author MinecraftPlaye
 */
public class LexiconData {
    
    public static LexiconChapter test;
    
    /**
     * Initializes the data for the
     * lexicon. This method has to be
     * called in the initialization
     * phase of FML.
     */
    public static void init() {
        LexiconData.test = new BasicChapter("lexicon").addAllPages(new PageText(0), 
                new PageText(1));
        
        for(int i = 0; i < 20; i++) {
            LexiconChapter c = new BasicChapter("lexicon" + i).addAllPages(new PageText(0), 
                    new PageText(1));
        }
    }
}