/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.worldgen;

import java.util.Random;

import minecraftplaye.justanotherenergy.common.lib.ModBlocks;
import minecraftplaye.justanotherenergy.common.lib.ModConfig;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkGenerator;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;

import net.minecraftforge.fml.common.IWorldGenerator;

public class ModWorldGen implements IWorldGenerator {
	
	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
	    if(!ModConfig.ENABLE_ORE_GEN) return;
	    
		if(world.provider.getDimension() == 0)
			this.generateOverworld(random, chunkX * 16, chunkZ * 16, world, chunkGenerator, chunkProvider);
		else if(world.provider.getDimension() == -1)
		    this.generateNether(random, chunkX * 16, chunkZ * 16, world, chunkGenerator, chunkProvider);
		else if(world.provider.getDimension() == 1)
		    this.generateTheEnd(random, chunkX * 16, chunkZ * 16, world, chunkGenerator, chunkProvider);
	}
	
	private void generateOverworld(Random random, int posX, int posZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
		this.generateOre(ModBlocks.ore_copper.getDefaultState(), world, random, posX, posZ, 10, 30, 24, 4);
		this.generateOre(ModBlocks.ore_tin.getDefaultState(), world, random, posX, posZ, 20, 55, 24, 4);
	}
	
	private void generateNether(Random random, int posX, int posZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
		
	}
	
	private void generateTheEnd(Random random, int posX, int posZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
		
	}
	
	/**
	 * Generates ore in the world between minY and maxY.
	 * 
	 * @param ore The ore to generate.
	 * @param world The world to generate in.
	 * @param random The random to use for generation.
	 * @param x The x position to generate the block at.
	 * @param z The z position to generate the block at.
	 * @param minY The minimum Y position for which the ore can be generated.
	 * @param maxY The maximum Y position for which the ore can be generated.
	 * @param size The size of each ore vein.
	 * @param chances The number of veins per chunk.
	 */
	private void generateOre(IBlockState ore, World world, Random random, int x, int z, int minY, int maxY, int size, int chances) {
		int deltaY = maxY - minY;
		
		for(int i = 0; i < chances; i++) {
			BlockPos pos = new BlockPos(x + random.nextInt(16), minY + random.nextInt(deltaY), z + random.nextInt(16));
			
			WorldGenMinable generator = new WorldGenMinable(ore, size);
			generator.generate(world, random, pos);
		}
	}
}