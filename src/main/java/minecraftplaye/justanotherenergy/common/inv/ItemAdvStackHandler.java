/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.inv;

import javax.annotation.Nullable;

import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;

import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;

/**
 * A basic implementation of the Forge Inventory API defined in {@link IItemHandler} 
 * and {@link IItemHandlerModifiable}. The Forge Inventory API is used in order 
 * to unify the Minecraft Inventory API, thus providing a better and easier way
 * for mods to interact with each others inventory. 
 * <p>
 * All entities (in general) of any kind are required to hold an instance of this class
 * to be able to deal with inventory related tasks.
 * <p>
 * This implementation comes with support to find out if this object has support
 * for the capability and to get it.
 * 
 * @author MinecraftPlaye
 */
public class ItemAdvStackHandler extends ItemStackHandler {
    
    /**
     * Creates a new instance of this manager to allow
     * an inventory to be used for whatever entity might
     * has implemented this. The default size of an inventory
     * is always 1 slot.
     */
    public ItemAdvStackHandler() {
        this(1);
    }
    
    /**
     * Creates a new instance of this manager to allow
     * an inventory to be used for whatever entity might
     * has implemented this.
     * 
     * @param size the size of the inventory in slots, e.g. a furnace has 3 slots
     */
    public ItemAdvStackHandler(int size) {
        super(size);
    }
    
    /**
     * Creates a new instance of this manager to allow
     * an inventory to be used for whatever entity might
     * has implemented this.
     * 
     * @param stacks a list containing all item stacks the inventory might hold upon creation
     */
    public ItemAdvStackHandler(NonNullList<ItemStack> stacks) {
        super(stacks);
    }
    
    /**
     * Retrieves the handler for the capability requested on the specific side. 
     * The return value CAN be null if the object does not support the capability. 
     * The return value CAN be the same for multiple faces.
     * 
     * @param capability the capability to request
     * @param facing the side to request the capability on
     * @return the handler for the requested capability or null
     */
    @Nullable
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
            return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(this);
        else
            return null;
    }
    
    /**
     * Determines if this object has support for the capability in question on the specific 
     * side. The return value of this <b>might<b> change during runtime if this object gains 
     * or looses support for a capability.
     * <p>
     * Example: A Pipe getting a cover placed on one side causing it loose the Inventory 
     * attachment function for that side. This is a light weight version of 
     * getCapability, intended for metadata uses.
     * 
     * @param capability the capability to check support for
     * @param facing the side of the capability 
     * @return true if the object has support for the capability on the specific side
     */
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;
    }
}