package minecraftplaye.justanotherenergy.common.inv;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import minecraftplaye.justanotherenergy.common.tileentities.TileEntityInventory;

import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;

import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

/**
 * 
 * @author MinecraftPlaye
 */
public class ItemUtils {
    
    public static @Nonnull ItemStack sendTo(@Nonnull TileEntity from, @Nonnull TileEntity sendTo, @Nullable EnumFacing orientation, int slot, int amount) {
        if(from == null || sendTo == null || slot < 0 || amount <= 0)
            return ItemStack.EMPTY;
        
        EnumFacing side = orientation != null ? orientation.getOpposite() : (EnumFacing) null;
        
        if(from instanceof TileEntityInventory && sendTo instanceof TileEntityInventory) {
            TileEntityInventory fromInv = (TileEntityInventory) from;
            TileEntityInventory sendInv = (TileEntityInventory) sendTo;
            fromInv.markDirty();
            sendInv.markDirty();
            return sendInv.inventory.insertItem(slot, fromInv.inventory.extractItem(slot, amount, false), false);
        } else if(from.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, side) && sendTo.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, side)) {
            IItemHandler fromHandler = from.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, side);
            IItemHandler sendToHandler = sendTo.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, side);
            from.markDirty();
            sendTo.markDirty();
            return sendToHandler.insertItem(slot, fromHandler.extractItem(slot, amount, false), false);
        }
        
        return ItemStack.EMPTY;
    }
    
    /**
     * 
     * @param from
     * @param orientation
     * @param slot
     * @param amount
     * @return ItemStack extracted from the slot
     */
    public static @Nonnull ItemStack extractItems(@Nonnull TileEntity from, @Nullable EnumFacing orientation, int slot, int amount) {
        if(from == null || slot < 0 || amount <= 0)
            return ItemStack.EMPTY;
        
        EnumFacing side = orientation != null ? orientation.getOpposite() : (EnumFacing) null;
        
        if(from instanceof TileEntityInventory) {
            TileEntityInventory fromInv = (TileEntityInventory) from;
            fromInv.markDirty();
            return fromInv.inventory.extractItem(slot, amount, false);
        } else if(from.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, side)) {
            IItemHandler itemHandler = from.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, side);
            from.markDirty();
            return itemHandler.extractItem(slot, amount, false);
        }
        return ItemStack.EMPTY;
    }
    
    /**
     * 
     * @param sendTo
     * @param orientation
     * @param slot
     * @param stack
     * @return The remaining ItemStack that was not inserted.
     */
    public static @Nonnull ItemStack insertItems(@Nonnull TileEntity sendTo, @Nullable EnumFacing orientation, int slot, ItemStack stack) {
        if(sendTo == null || slot < 0 || stack == null || stack == ItemStack.EMPTY)
            return ItemStack.EMPTY;
        
        EnumFacing side = orientation != null ? orientation.getOpposite() : (EnumFacing) null;
        
        if(sendTo instanceof TileEntityInventory) {
            TileEntityInventory sendInv = (TileEntityInventory) sendTo;
            sendInv.markDirty();
            return sendInv.inventory.insertItem(slot, stack, false);
        } else if(sendTo.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, side)) {
            IItemHandler itemHandler = sendTo.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, side);
            sendTo.markDirty();
            return itemHandler.insertItem(slot, stack, false);
        }
        return ItemStack.EMPTY;
    }
}