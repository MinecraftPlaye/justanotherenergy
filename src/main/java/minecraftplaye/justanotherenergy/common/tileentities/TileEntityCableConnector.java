/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.tileentities;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Queue;

import org.apache.commons.lang3.tuple.Pair;

import minecraftplaye.justanotherenergy.api.gui.IRenderOverlay;
import minecraftplaye.justanotherenergy.client.gui.UIEventHandler;
import minecraftplaye.justanotherenergy.common.power.EnergyManager.EnergyTransfer;
import minecraftplaye.justanotherenergy.common.power.EnergyUtils;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntityCableConnector extends TileEntityEnergy implements IRenderOverlay {
    
    private boolean flag = false;
    public boolean shouldRecalculate = false;
    private HashMap<BlockPos, EnumFacing> connected = new HashMap<>(); // without pre-calculated weight
    
    public TileEntityCableConnector() {
        super(2500, 2000);
        this.container.setTransferMode(EnergyTransfer.PRODUCER);
    }
    
    @Override
    public void update() {
    	super.update();
    	if(this.getWorld().isRemote || !this.hasWorld())
    		return;
    	
        this.flag = false;
        if(this.shouldRecalculate || this.connected.isEmpty()) {
            this.shouldRecalculate = false;
            this.findTransferPipes();
            this.flag = true;
        }
        
        /*
         * send energy to connected machines which consume energy
         * a connection is established when this instance is connected to another
         * machine directly or using the TileEntityCable class
         */
        this.connected.forEach((pos, side) -> {
            final TileEntity tile = this.getWorld().getTileEntity(pos);
            if(tile == null) return;
            
            int send = EnergyUtils.sendEnergy(this, tile, side, this.getEnergyManager().getEnergyStored() / this.connected.size());
            if(send <= 0) {
            	send = EnergyUtils.sendEnergy(this, tile, null, this.getEnergyManager().getEnergyStored() / this.connected.size());
            	this.flag = send > 0;
            }
        });
        
        if(this.flag)
        	this.markForUpdates();
    }
    
    /**
     * This method is used to find all connected transfer pipes, whether
     * they are directly connected or connected via the TileEntityPipeEnergy.
     * As such a search through the entire cable network costs a lot
     * of performance it is recommended to call this method only
     * when absolutely necessary.
     * <p>
     * All connections will be added to the <code>connected</code>
     * HashMap for further usage.
     */
    private void findTransferPipes() {
        if(this.connected.size() > 0)
            this.connected.clear();
        
        // we want to store the facing and the position
        Queue<Pair<BlockPos, EnumFacing>> toSearch = new ArrayDeque<>();
        HashSet<BlockPos> scanned = new HashSet<BlockPos>();
        scanned.add(this.getPos());
        
        if(toSearch.isEmpty() || toSearch.peek() == null)
            this.getBlocksToScan(toSearch, scanned, this.getPos());
        
        // temp object because we poll stuff out of the list
        Pair<BlockPos, EnumFacing> pair = null;
        BlockPos current = null;
        EnumFacing face = null;
        
        while(toSearch.peek() != null) {
            pair = toSearch.poll();
            current = pair.getLeft();
            face = pair.getRight();
            scanned.add(current);
            
            final TileEntity tile = this.getWorld().getTileEntity(current);
            if(tile == null)
                continue;
            else if(tile instanceof TileEntityCable) // check cable connection
                this.getBlocksToScan(toSearch, scanned, current);
            else if(tile instanceof TileEntityCableConnector)
                this.connected.put(current, face);
            else if(tile instanceof TileEntityEnergy) {
                TileEntityEnergy energy = (TileEntityEnergy) tile;
                if(energy.container.getTransferMode() == EnergyTransfer.PRODUCER)
                    continue;
                else if(energy.container.getTransferMode() == EnergyTransfer.CONSUMER || energy.container.getTransferMode() == EnergyTransfer.BOTH)
                    this.connected.put(current, face);
            } else {
                if(EnergyUtils.hasCapability(tile, face))
                    this.connected.put(current, face);
            }
        }
    }
    
    /**
     * Searches for surrounding blocks which aren't in the <code>scanned</code> set and adds them
     * to the <code>toSearch</code> Queue. The starting point of this search in all directions
     * is the block position which has to be passed as a parameter to this method.
     * The facing of the found block will also be added to the <code>toSearch</code> Queue.
     * <b>This may include the facing null!</b>
     * 
     * @param toSearch the Queue to add the results to
     * @param scanned defines which block positions should not be added to the Queue
     * @param pos the position to begin the search from
     */
    private void getBlocksToScan(Queue<Pair<BlockPos, EnumFacing>> toSearch, HashSet<BlockPos> scanned, BlockPos pos) {
        // EnumSet.allOf(EnumFacing.class) does not include the null, so machines which don't require a specific
        // site will be ignored
        for(EnumFacing face : EnumFacing.VALUES) {
            // same as pos.north(), just for all directions
            BlockPos offset = pos.offset(face);
            if(!scanned.contains(offset))
                // create a new pair and add it to the list
                // a pair is just some class which holds two fields
                toSearch.add(Pair.of(offset, face.getOpposite()));
        }
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
    	if(compound.hasKey("connection")) {
            NBTTagList list = compound.getTagList("connection", Constants.NBT.TAG_COMPOUND);
            for(int i = 0; i <= list.tagCount(); i++) {
                NBTTagCompound com = list.getCompoundTagAt(i);
                if(com.hasKey("posX") && com.hasKey("posY") && com.hasKey("posZ")) {
                	EnumFacing facing = null;
                	if(com.hasKey("facing"))
                		facing = EnumFacing.byName(com.getString("facing"));
                	
                    this.connected.put(
                            new BlockPos(
                                    com.getInteger("posX"),
                                    com.getInteger("posY"),
                                    com.getInteger("posZ")), 
                            		facing);
                }
            }
        }
    }
    
    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
    	NBTTagList list = new NBTTagList();
        this.connected.forEach((pos, side) -> {
            NBTTagCompound com = new NBTTagCompound();
            com.setInteger("posX", pos.getX());
            com.setInteger("posY", pos.getY());
            com.setInteger("posZ", pos.getZ());
            
            if(side != null && side.getName() != null)
            	com.setString("facing", side.getName());
            
            list.appendTag(com);
        });
        compound.setTag("connection", list);
        
        return super.writeToNBT(compound);
    }
    
    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.getPos(), 3, this.getUpdateTag());
    }
    
    @Override
    public NBTTagCompound getUpdateTag() {
        return this.writeToNBT(new NBTTagCompound());
    }
    
    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        super.onDataPacket(net, pkt);
        this.handleUpdateTag(pkt.getNbtCompound());
    }
    
    /**
     * Returns the map which holds all connections to this instance. This does not include machines.
     * 
     * @return Returns the map which holds all connections to this instance.
     */
    public HashMap<BlockPos, EnumFacing> getConnections() {
    	return this.connected;
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void renderEnergy(World worldIn) {
        UIEventHandler.renderEnergyHUD(worldIn, this.container.getEnergyStored(), this.container.getDisplayMaxStored(), 13510155);
    }
    
	@Override
	public void renderPollution(World worldIn) {
		// currently nothing in here
	}
}