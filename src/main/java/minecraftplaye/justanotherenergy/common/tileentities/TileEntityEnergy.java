/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.tileentities;

import java.util.EnumSet;

import javax.annotation.Nullable;

import minecraftplaye.justanotherenergy.common.power.EnergyManager;

import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;

import net.minecraftforge.common.capabilities.Capability;

public abstract class TileEntityEnergy extends TileEntity implements ITickable {
    
    public boolean shouldExplode = false;
    public final EnergyManager container;
    
    public TileEntityEnergy() {
    	this(0, 0);
	}
    
    public TileEntityEnergy(int capacity) {
        this(capacity, capacity);
    }
    
    public TileEntityEnergy(int capacity, boolean shouldExplode) {
        this(capacity, capacity, shouldExplode);
    }
    
    public TileEntityEnergy(int capacity, int maxTransfer) {
        this(capacity, maxTransfer, false);
    }
    
    public TileEntityEnergy(int capacity, int maxTransfer, boolean shouldExplode) {
        this(capacity, maxTransfer, maxTransfer, shouldExplode);
    }
    
    public TileEntityEnergy(int capacity, int maxReceive, int maxExtract) {
        this(capacity, maxReceive, maxExtract, false);
    }
    
    public TileEntityEnergy(int capacity, int maxReceive, int maxExtract, boolean shouldExplode) {
        this.shouldExplode = shouldExplode;
        this.container = new EnergyManager(capacity, maxReceive, maxExtract);
    }
    
    @Override
    public void update() {
        if(!this.hasWorld() || this.getWorld().isRemote)
            return;
        
        if(this.container.getEnergyStored() >= this.container.getMaxEnergyStored() && this.shouldExplode) {
            this.getWorld().createExplosion(null, (double)this.getPos().getX(), (double)this.getPos().getY(), (double)this.getPos().getZ(), 10.f, true);
            this.getWorld().setBlockState(this.getPos(), Blocks.AIR.getDefaultState());
        }
        
        if(this.container.hasChanged()) {
            this.container.resetHasChanged();
        	this.markForUpdates();
        }
    }
    
    public void markForUpdates() {
    	this.getWorld().markBlockRangeForRenderUpdate(this.getPos(), this.getPos());
    	IBlockState state = this.getWorld().getBlockState(this.getPos());
    	this.getWorld().notifyBlockUpdate(this.getPos(), state, state, 3);
    	this.getWorld().scheduleBlockUpdate(this.getPos(), this.getBlockType(), 0, 0);
    	this.markDirty();
    }
    
    public EnergyManager getEnergyManager() {
        return this.container;
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound) {
        this.container.deserializeNBT(compound.getCompoundTag("energy"));
        super.readFromNBT(compound);
    }
    
    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setTag("energy", this.container.serializeNBT());
        return super.writeToNBT(compound);
    }
    
    /**
     * Returns an EnumSet containing all sides the energy capability
     * is exposed to.
     * 
     * @return An EnumSet of all sides the energy capability is exposed to
     */
    protected EnumSet<EnumFacing> getExposedSides() {
        EnumSet<EnumFacing> facing = EnumSet.allOf(EnumFacing.class);
        return facing;
    }
    
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        T cap = this.container.getCapability(capability, facing);
        if(cap != null && facing == null)
            return cap;
        
        return cap != null && this.getExposedSides().contains(facing) ? cap : super.getCapability(capability, facing);
    }
    
    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability != null ? 
                this.container.hasCapability(capability, facing) && (this.getExposedSides().contains(facing) || facing == null) :
                super.hasCapability(capability, facing);
    }
}