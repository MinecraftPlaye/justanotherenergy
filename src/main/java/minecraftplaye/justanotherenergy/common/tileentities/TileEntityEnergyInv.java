/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.tileentities;

import java.util.EnumSet;

import minecraftplaye.justanotherenergy.common.inv.ItemAdvStackHandler;
import minecraftplaye.justanotherenergy.common.power.EnergyManager;

import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;

import net.minecraftforge.common.capabilities.Capability;

public class TileEntityEnergyInv extends TileEntity implements ITickable {
    
    public boolean shouldExplode = false;
    public final EnergyManager container;
    public final ItemAdvStackHandler inventory;
    
    public TileEntityEnergyInv() {
        this(0, 0);
    }
    
    public TileEntityEnergyInv(int capacity) {
        this(capacity, 1);
    }
    
    public TileEntityEnergyInv(int capacity, int size) {
        this(capacity, false, size);
    }
    
    public TileEntityEnergyInv(int capacity, boolean shouldExplode) {
        this(capacity, capacity, shouldExplode, 1);
    }
    
    public TileEntityEnergyInv(int capacity, boolean shouldExplode, int size) {
        this(capacity, capacity, capacity, shouldExplode, size);
    }
    
    public TileEntityEnergyInv(int capacity, int maxTransfer, boolean shouldExplode, int size) {
        this(capacity, maxTransfer, maxTransfer, shouldExplode, size);
    }
    
    public TileEntityEnergyInv(int capacity, int maxReceive, int maxExtract) {
        this(capacity, maxReceive, maxExtract, false, 1);
    }
    
    public TileEntityEnergyInv(int capacity, int maxReceive, int maxExtract, int size) {
        this(capacity, maxReceive, maxExtract, false, size);
    }
    
    public TileEntityEnergyInv(int capacity, int maxReceive, int maxExtract, boolean shouldExplode, int size) {
        this.shouldExplode = shouldExplode;
        this.inventory = new ItemAdvStackHandler();
        this.container = new EnergyManager(capacity, maxReceive, maxExtract);
    }
    
    @Override
    public void update() {
        if(!this.hasWorld() || this.getWorld().isRemote)
            return;
        
        if(this.container.getEnergyStored() >= this.container.getMaxEnergyStored() && this.shouldExplode) {
            this.getWorld().createExplosion(null, (double)this.getPos().getX(), (double)this.getPos().getY(), (double)this.getPos().getZ(), 10.f, true);
            this.getWorld().setBlockState(this.getPos(), Blocks.AIR.getDefaultState());
        }
        
        if(this.container.hasChanged()) {
            this.container.resetHasChanged();
            this.markForUpdates();
        }
    }
    
    public void markForUpdates() {
        this.getWorld().markBlockRangeForRenderUpdate(this.getPos(), this.getPos());
        IBlockState state = this.getWorld().getBlockState(this.getPos());
        this.getWorld().notifyBlockUpdate(this.getPos(), state, state, 3);
        this.getWorld().scheduleBlockUpdate(this.getPos(), this.getBlockType(), 0, 0);
        this.markDirty();
    }
    
    public EnergyManager getEnergyManager() {
        return this.container;
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound) {
        this.container.deserializeNBT(compound.getCompoundTag("energy"));
        this.inventory.deserializeNBT(compound.getCompoundTag("inventory"));
        super.readFromNBT(compound);
    }
    
    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setTag("energy", this.container.serializeNBT());
        compound.setTag("inventory", this.inventory.serializeNBT());
        return super.writeToNBT(compound);
    }
    
    /**
     * Returns an EnumSet containing all sides the energy capability
     * is exposed to.
     * 
     * @return An EnumSet of all sides the energy capability is exposed to
     */
    protected EnumSet<EnumFacing> getExposedPowSides() {
        EnumSet<EnumFacing> facing = EnumSet.allOf(EnumFacing.class);
        return facing;
    }
    
    /**
     * Returns an EnumSet containing all sides the inventory capability
     * is exposed to.
     * 
     * @return An EnumSet of all sides the inventory capability is exposed to
     */
    protected EnumSet<EnumFacing> getExposedInvSides() {
        EnumSet<EnumFacing> facing = EnumSet.allOf(EnumFacing.class);
        return facing;
    }
    
    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
        T powCap = this.container.getCapability(capability, facing);
        T invCap = this.inventory.getCapability(capability, facing);
        if(powCap != null) {
            if(facing == null) return powCap;
            if(this.getExposedPowSides().contains(facing)) return powCap;
        }
        if(invCap != null) {
            if(facing == null) return powCap;
            if(this.getExposedInvSides().contains(facing)) return invCap;
        }
        return super.getCapability(capability, facing);
    }
    
    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
        if(capability != null) {
            if(this.container.hasCapability(capability, facing))
                if(this.getExposedPowSides().contains(facing) || facing == null)
                    return true;
            if(this.inventory.hasCapability(capability, facing))
                if(this.getExposedInvSides().contains(facing) || facing == null)
                    return true;
        }
        return super.hasCapability(capability, facing);
    }
}