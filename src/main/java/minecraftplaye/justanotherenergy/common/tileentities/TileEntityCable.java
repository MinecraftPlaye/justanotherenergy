/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.tileentities;

import java.util.ArrayDeque;
import java.util.EnumSet;
import java.util.HashSet;

import minecraftplaye.justanotherenergy.common.power.EnergyUtils;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

import net.minecraftforge.common.util.Constants;

public class TileEntityCable extends TileEntity {
	
    private HashSet<BlockPos> connected_machines = new HashSet<>();
    
    public void searchNetwork() {
        if(this.connected_machines.size() > 0)
            this.connected_machines.clear();
        
        ArrayDeque<BlockPos> toSearch = new ArrayDeque<>();
        HashSet<BlockPos> scanned = new HashSet<>();
        
        if(toSearch.isEmpty() || toSearch.peek() == null)
            this.getBlocksToScan(toSearch, scanned, this.getPos());
        
        BlockPos current = null;
        
        while(toSearch.peek() != null) {
            current = toSearch.pop();
            scanned.add(current);
            
            TileEntity tile = this.getWorld().getTileEntity(current);
            if(tile != null) {
                if(tile instanceof TileEntityCable)
                    this.getBlocksToScan(toSearch, scanned, current);
                else if(tile instanceof TileEntityCableConnector)
                    this.connected_machines.add(current);
                else if(tile instanceof TileEntityEnergy)
                    continue;
                else {
                    if(EnergyUtils.hasCapability(tile, null))
                        continue;
                }
            }
        }
    }
    
    private void getBlocksToScan(ArrayDeque<BlockPos> toSearch, HashSet<BlockPos> scanned, BlockPos pos) {
        for(EnumFacing face : EnumSet.allOf(EnumFacing.class)) {
            // same as pos.north(), just for all directions
            BlockPos offset = pos.offset(face);
            if(!scanned.contains(offset))
                toSearch.add(offset);
        }
    }
    
    public void informNetwork() {
        this.connected_machines.forEach(pos -> {
            TileEntity tile = this.getWorld().getTileEntity(pos);
            // make sure the tile entity at 'pos' is really the one we think it is
            if(tile != null && tile instanceof TileEntityCableConnector) {
                TileEntityCableConnector transfer = (TileEntityCableConnector) tile;
                transfer.shouldRecalculate = true;
            }
        });
    }
    
    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        NBTTagList list = new NBTTagList();
        for(BlockPos pos : this.connected_machines) {
            NBTTagCompound com = new NBTTagCompound();
            com.setInteger("posX", pos.getX());
            com.setInteger("posY", pos.getY());
            com.setInteger("posZ", pos.getZ());
            list.appendTag(com);
        }
        compound.setTag("machines", list);
        
        return super.writeToNBT(compound);
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        
        if(compound.hasKey("machines")) {
            NBTTagList list = compound.getTagList("machines", Constants.NBT.TAG_COMPOUND);
            for(int i = 0; i <= list.tagCount(); i++) {
                NBTTagCompound com = list.getCompoundTagAt(i);
                if(com.hasKey("posX") && com.hasKey("posY") && com.hasKey("posZ")) {
                    
                    this.connected_machines.add(
                            new BlockPos(
                                    com.getInteger("posX"),
                                    com.getInteger("posY"),
                                    com.getInteger("posZ"))
                            );
                }
            }
        }
    }
}