/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.tileentities;

import minecraftplaye.justanotherenergy.common.power.EnergyManager.EnergyTransfer;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.world.World;

import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.ForgeChunkManager.Ticket;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import minecraftplaye.justanotherenergy.JustAnotherEnergy;
import minecraftplaye.justanotherenergy.api.gui.IRenderOverlay;
import minecraftplaye.justanotherenergy.client.gui.UIEventHandler;
import minecraftplaye.justanotherenergy.common.lib.ChunkLoaderManager;
import minecraftplaye.justanotherenergy.common.power.EnergyUtils;

public class TileEntityChunkLoader extends TileEntityEnergy implements IRenderOverlay {
    
    @Nullable
    private UUID ownerId = null;
    private static final String UUID_TAG = "UUID";
    
    @Nullable
    private ForgeChunkManager.Ticket ticket = null;
    
    /*
     *  ticket used as a default to stop conflicts with changing the actual ticket
     *  implemented to allow the block to not call ChunkLoaderManager.startChunkLoading!
     */
    @Nullable
    private ForgeChunkManager.Ticket dfTicket = null;
    
    private boolean wasActive = false;
    
    public TileEntityChunkLoader() {
    	// this has to be here in order for correct saving and loading of NBT data
	}
    
    public TileEntityChunkLoader(int capacity, int maxReceive, int maxExtract) {
        super(capacity, maxReceive, maxExtract, false);
        
        this.getEnergyManager().setTransferMode(EnergyTransfer.CONSUMER);
    }
    
    @Override
    public void update() {
        super.update();
        
        if(!this.hasWorld() || this.getWorld().isRemote || this.ticket == null)
            return;
        
        boolean flag = false;
        flag = EnergyUtils.consumeEnergy(this, 100);
        
        if(flag) {
            if(this.wasActive)
                ChunkLoaderManager.startChunkLoading(this.getWorld(), this.ticket);
                //ChunkLoaderManager.startChunkLoading(this.getWorld(), this.dfTicket);
            
            this.getWorld().notifyBlockUpdate(this.getPos(), this.getWorld().getBlockState(this.getPos()), this.getWorld().getBlockState(this.getPos()), 3);
            this.markDirty();
        } else if(this.ticket != null && !flag && !this.wasActive)
            ChunkLoaderManager.stopChunkLoading(this.getWorld(), this.ticket);
        
        if(this.wasActive != flag)
            this.wasActive = flag;
    }
    
    public void setDefaultTicket(@Nonnull Ticket ticket) {
        this.dfTicket = ticket;
    }
    
    public void setTicket(@Nonnull Ticket ticket) {
    	if(this.ticket != null)
    		ForgeChunkManager.releaseTicket(this.ticket);
    	
    	this.ticket = ticket;
    	this.getWorld().addBlockEvent(this.getPos(), this.getBlockType(), 1, 1);
    	JustAnotherEnergy.getLogger().info("Reloading Chunk Loader at {} because {} placed or interacted with it.", this.getPos(), this.ticket.getPlayerName());
    }
    
    public Ticket getTicket() {
    	return this.ticket;
    }
    
    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.getPos(), 3, this.getUpdateTag());
    }
    
    @Override
    public NBTTagCompound getUpdateTag() {
        return this.writeToNBT(new NBTTagCompound());
    }
    
    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        super.onDataPacket(net, pkt);
        this.handleUpdateTag(pkt.getNbtCompound());
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound) {
    	if(compound.hasKey(TileEntityChunkLoader.UUID_TAG))
    		this.ownerId = compound.getUniqueId(TileEntityChunkLoader.UUID_TAG);
        
        super.readFromNBT(compound);
    }
    
    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
    	if(this.ownerId != null)
    		compound.setUniqueId(TileEntityChunkLoader.UUID_TAG, this.ownerId);
        
        return super.writeToNBT(compound);
    }
    
    public void setOwnerId(UUID ownerId) {
        this.ownerId = ownerId;
    }
    
    public String getOwnerIdString() {
    	return this.ownerId.toString();
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void renderEnergy(World worldIn) {
        UIEventHandler.renderEnergyHUD(worldIn, this.container.getEnergyStored(), this.container.getDisplayMaxStored(), 13510155);
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void renderPollution(World worldIn) {
        // Nothing to render here
    }
}
