/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.tileentities;

import minecraftplaye.justanotherenergy.common.power.EnergyManager.EnergyTransfer;

import net.minecraft.util.ITickable;

public class TileEntityCapacitorBank extends TileEntityEnergy implements ITickable {
    
    public TileEntityCapacitorBank() {
        super(25000, 250);
        this.container.setTransferMode(EnergyTransfer.CONSUMER);
    }
}