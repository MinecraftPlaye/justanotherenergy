/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.tileentities;

import java.util.EnumSet;

import minecraftplaye.justanotherenergy.common.inv.ItemAdvStackHandler;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;

import net.minecraftforge.common.capabilities.Capability;

public abstract class TileEntityInventory extends TileEntity {
    
    public final ItemAdvStackHandler inventory;
    
    public TileEntityInventory() {
        this(1);
    }
    
    /**
     * Creates a basic inventory handling tile entity.
     * 
     * @param size how many inventory slots the inventory should have
     */
    public TileEntityInventory(int size) {
        this.inventory = new ItemAdvStackHandler(size);
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound) {
        this.inventory.deserializeNBT(compound.getCompoundTag("inventory"));
        super.readFromNBT(compound);
    }
    
    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setTag("inventory", this.inventory.serializeNBT());
        return super.writeToNBT(compound);
    }
    
    /**
     * Returns an EnumSet containing all sides the inventory capability
     * is exposed to.
     * 
     * @return An EnumSet of all sides the inventory capability is exposed to
     */
    protected EnumSet<EnumFacing> getExposedSides() {
        EnumSet<EnumFacing> facing = EnumSet.allOf(EnumFacing.class);
        return facing;
    }
    
    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
        T cap = this.inventory.getCapability(capability, facing);
        if(cap != null && facing == null)
            return cap;
        
        return cap != null && this.getExposedSides().contains(facing) ? cap : super.getCapability(capability, facing);
    }
    
    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
        return capability != null ?
                this.inventory.hasCapability(capability, facing) && (this.getExposedSides().contains(facing) || facing == null) :
                super.hasCapability(capability, facing);
    }
}