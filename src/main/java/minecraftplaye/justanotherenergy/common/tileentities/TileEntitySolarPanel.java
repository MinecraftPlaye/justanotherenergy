/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.tileentities;

import java.util.EnumSet;

import minecraftplaye.justanotherenergy.api.gui.IRenderOverlay;
import minecraftplaye.justanotherenergy.client.gui.UIEventHandler;
import minecraftplaye.justanotherenergy.common.power.EnergyManager.EnergyTransfer;
import minecraftplaye.justanotherenergy.common.power.EnergyUtils;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntitySolarPanel extends TileEntityEnergy implements IRenderOverlay {
    
    private int inputRate;
    private boolean twoTick = false;
    
    public TileEntitySolarPanel() {
        super(10);
        this.getEnergyManager().setTransferMode(EnergyTransfer.PRODUCER);
    }
    
    @Override
    public void update() {
        super.update();
        boolean flag = false;
        
        if(!this.hasWorld() || this.getWorld().isRemote)
            return;
        
        this.twoTick = !this.twoTick;
        
        if(this.getWorld().provider.hasSkyLight() && this.getWorld().canBlockSeeSky(this.getPos())) {
            if(this.getWorld().isDaytime() && !this.getWorld().isRaining()) this.inputRate = 108;
            else this.inputRate = this.twoTick ? 64 : 0;
            flag = true;
        }
        
        this.container.receiveEnergy(this.inputRate, false);
        
        if(this.world.getTileEntity(this.getPos().down()) != null) {
            int received = EnergyUtils.sendEnergy(this, this.world.getTileEntity(this.getPos().down()), EnumFacing.DOWN, 10);
            
            if(received > 0) flag = true;
        }
        
        if(flag) {
            this.getWorld().notifyBlockUpdate(this.getPos(), this.getWorld().getBlockState(this.getPos()), this.getWorld().getBlockState(this.getPos()), 3);
            this.markDirty();
        }
    }
    
    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.getPos(), 3, this.getUpdateTag());
    }
    
    @Override
    public NBTTagCompound getUpdateTag() {
        return this.writeToNBT(new NBTTagCompound());
    }
    
    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        super.onDataPacket(net, pkt);
        this.handleUpdateTag(pkt.getNbtCompound());
    }
    
    /**
     * @param id 
     * @return the field
     * @deprecated Replaced with new UI system from {@link net.minecraftforge.client.event.RenderGameOverlayEvent.Post} and
     * {@link UIEventHandler}.
     */
    @Deprecated
    public int getField(int id) {
        if(id == 0) return this.container.getEnergyStored();
        else if(id == 1) return this.inputRate;
        else return 0;
    }
    
    /**
     * @param id 
     * @param value 
     * @deprecated Replaced with new UI system from {@link net.minecraftforge.client.event.RenderGameOverlayEvent.Post} and
     * {@link UIEventHandler}.
     */
    @Deprecated
    public void setField(int id, int value) {
        if(id == 0) this.container.setEnergyStored(value);
        else if(id == 1) this.inputRate = value;
    }
    
    /**
     * @param player 
     * @return if it can be used
     * @deprecated Replaced with new UI system from {@link net.minecraftforge.client.event.RenderGameOverlayEvent.Post} and
     * {@link UIEventHandler}.
     */
    @Deprecated
    public boolean isUsableByPlayer(EntityPlayer player) {
        return this.getWorld().getTileEntity(this.getPos()) != this ? false : 
                        player.getDistanceSq((double)this.getPos().getX() + .5d, 
                        (double)this.getPos().getY() + .5d, 
                        (double)this.getPos().getZ() + .5d) <= 64.d;
    }
    
    @Override
    protected EnumSet<EnumFacing> getExposedSides() {
        return EnumSet.of(EnumFacing.DOWN);
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void renderEnergy(World worldIn) {
        UIEventHandler.renderEnergyHUD(worldIn, this.container.getEnergyStored(), this.container.getDisplayMaxStored(), 13510155);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void renderPollution(World worldIn) {
        // Nothing to render here
    }
}
