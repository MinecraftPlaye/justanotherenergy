/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.power.tesla;

import javax.annotation.Nullable;

import minecraftplaye.justanotherenergy.JustAnotherEnergy;
import minecraftplaye.justanotherenergy.common.lib.Constants;

import net.darkhax.tesla.api.ITeslaConsumer;
import net.darkhax.tesla.api.ITeslaHolder;
import net.darkhax.tesla.api.ITeslaProducer;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;

import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.fml.common.Optional;

public class TeslaHelper {
    
    @Nullable
    @CapabilityInject(ITeslaConsumer.class)
    public static final Capability<ITeslaConsumer> TESLA_CONSUMER = null;
    
    @Nullable
    @CapabilityInject(ITeslaProducer.class)
    public static final Capability<ITeslaProducer> TESLA_PRODUCER = null;
    
    @Nullable
    @CapabilityInject(ITeslaHolder.class)
    public static final Capability<ITeslaHolder> TESLA_HOLDER = null;
    
    public static boolean isLoaded() {
        return TESLA_CONSUMER != null && TESLA_HOLDER != null && TESLA_PRODUCER != null;
    }
    
    public static boolean isEnergyReceiver(TileEntity tile, @Nullable EnumFacing side) {
        return TeslaHelper.isLoaded() && _isEnergyReceiver(tile, side);
    }
    
    public static boolean isEnergyProducer(TileEntity tile, @Nullable EnumFacing side) {
        return TeslaHelper.isLoaded() && _isEnergyProducer(tile, side);
    }
    
    public static boolean isEnergyHolder(TileEntity tile, @Nullable EnumFacing side) {
        return TeslaHelper.isLoaded() && _isEnergyHolder(tile, side);
    }
    
    public static int sendEnergy(TileEntity tile, @Nullable EnumFacing side, int amount, boolean simulated) {
        return TeslaHelper.isLoaded() ? _sendEnergy(tile, side, amount, simulated) : 0;
    }
    
    /**
     * Converts from long to int. If the long is larger then the
     * maximum integer value, the long will be decreaded until
     * it is in scope.
     * 
     * @param power The power to convert.
     * @return power as an integer
     */
    public static int getIntPower(long power) {
        if(power > Integer.MAX_VALUE)
            return Integer.MAX_VALUE;
        if(power < Integer.MIN_VALUE)
            return Integer.MIN_VALUE;
        return (int) power;
    }
    
    @Optional.Method(modid = Constants.MODID_TESLA)
    private static boolean _isEnergyReceiver(TileEntity tile, @Nullable EnumFacing side) {
        return TeslaHelper.TESLA_CONSUMER != null && tile.hasCapability(TeslaHelper.TESLA_CONSUMER, side);
    }
    
    @Optional.Method(modid = Constants.MODID_TESLA)
    private static boolean _isEnergyProducer(TileEntity tile, @Nullable EnumFacing side) {
        return TeslaHelper.TESLA_PRODUCER != null && tile.hasCapability(TeslaHelper.TESLA_PRODUCER, side);
    }
    
    @Optional.Method(modid = Constants.MODID_TESLA)
    private static boolean _isEnergyHolder(TileEntity tile, @Nullable EnumFacing side) {
        return TeslaHelper.TESLA_HOLDER != null && tile.hasCapability(TeslaHelper.TESLA_HOLDER, side);
    }
    
    @Optional.Method(modid = Constants.MODID_TESLA)
    private static int _sendEnergy(TileEntity tile, @Nullable EnumFacing side, int amount, boolean simulated) {
        ITeslaConsumer consumer = tile.getCapability(TeslaHelper.TESLA_CONSUMER, side);
        if(consumer == null) {
            if(tile.hasCapability(TeslaHelper.TESLA_CONSUMER, side))
                JustAnotherEnergy.LOGGER.error("Tile claims to support Tesla but does not have the capability. {} {}", tile.getPos(), tile);
            return 0;
        }
        return (int) consumer.givePower(amount, simulated);
    }
}