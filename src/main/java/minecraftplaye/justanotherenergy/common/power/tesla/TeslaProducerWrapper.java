/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.power.tesla;

import minecraftplaye.justanotherenergy.common.lib.Constants;
import minecraftplaye.justanotherenergy.common.power.EnergyManager;
import net.darkhax.tesla.api.ITeslaProducer;
import net.minecraftforge.fml.common.Optional;

@Optional.Interface(iface = "net.darkhax.tesla.api.ITeslaProducer", modid = Constants.MODID_TESLA)
public class TeslaProducerWrapper implements ITeslaProducer {
    
    private final EnergyManager manager;
    
    public TeslaProducerWrapper(EnergyManager manager) {
        this.manager = manager;
    }
    
    @Override
    @Optional.Method(modid = Constants.MODID_TESLA)
    public long takePower(long power, boolean simulated) {
        int clampPower = TeslaHelper.getIntPower(power);
        return this.manager.extractEnergy(clampPower, simulated);
    }
}