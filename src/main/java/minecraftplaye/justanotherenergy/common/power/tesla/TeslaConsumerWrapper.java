/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.power.tesla;

import minecraftplaye.justanotherenergy.common.lib.Constants;
import minecraftplaye.justanotherenergy.common.power.EnergyManager;
import net.darkhax.tesla.api.ITeslaConsumer;

import net.minecraftforge.fml.common.Optional;

@Optional.Interface(iface = "net.darkhax.tesla.api.ITeslaConsumer", modid = Constants.MODID_TESLA)
public class TeslaConsumerWrapper implements ITeslaConsumer {
    
    private final EnergyManager manager;
    
    public TeslaConsumerWrapper(EnergyManager manager) {
        this.manager = manager;
    }
    
    @Override
    @Optional.Method(modid = Constants.MODID_TESLA)
    public long givePower(long power, boolean simulated) {
        int clampPower = TeslaHelper.getIntPower(power);
        return this.manager.receiveEnergy(clampPower, simulated);
    }
}