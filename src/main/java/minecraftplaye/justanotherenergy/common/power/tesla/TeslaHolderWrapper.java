/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.power.tesla;

import minecraftplaye.justanotherenergy.common.lib.Constants;
import minecraftplaye.justanotherenergy.common.power.EnergyManager;
import net.darkhax.tesla.api.ITeslaHolder;

import net.minecraftforge.fml.common.Optional;

@Optional.Interface(iface = "net.darkhax.tesla.api.ITeslaHolder", modid = Constants.MODID_TESLA)
public class TeslaHolderWrapper implements ITeslaHolder {
    
    private final EnergyManager manager;
    
    public TeslaHolderWrapper(EnergyManager manager) {
        this.manager = manager;
    }
    
    @Override
    @Optional.Method(modid = Constants.MODID_TESLA)
    public long getStoredPower() {
        return this.manager.getEnergyStored();
    }
    
    @Override
    @Optional.Method(modid = Constants.MODID_TESLA)
    public long getCapacity() {
        return this.manager.getMaxEnergyStored();
    }
}