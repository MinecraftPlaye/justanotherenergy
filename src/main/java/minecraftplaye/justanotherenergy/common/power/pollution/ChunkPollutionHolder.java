/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.power.pollution;

import java.util.HashMap;

import minecraftplaye.justanotherenergy.api.pollution.IChunkPollution;
import minecraftplaye.justanotherenergy.api.pollution.IChunkPollutionHolder;
import net.minecraft.util.math.ChunkPos;

/**
 * Default implementation of {@link IChunkPollutionHolder}.
 * 
 * @author MinecraftPlaye
 */
public class ChunkPollutionHolder implements IChunkPollutionHolder {
    
    private final HashMap<ChunkPos, IChunkPollution> chunkEntries = new HashMap<>();
    
    @Override
    public IChunkPollution getChunkPollution(ChunkPos pos) {
        return chunkEntries.get(pos);
    }
    
    @Override
    public void setChunkPollution(ChunkPos pos, IChunkPollution pollution) {
        this.chunkEntries.put(pos, pollution);
    }
    
    @Override
    public void removeChunkPollution(ChunkPos pos) {
        this.chunkEntries.remove(pos);
    }
}