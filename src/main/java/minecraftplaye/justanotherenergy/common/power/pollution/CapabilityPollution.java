/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.power.pollution;

import minecraftplaye.justanotherenergy.api.pollution.IChunkPollution;
import minecraftplaye.justanotherenergy.api.pollution.IChunkPollutionHolder;
import minecraftplaye.justanotherenergy.common.lib.Constants;
import minecraftplaye.justanotherenergy.common.network.NetworkHandler;
import minecraftplaye.justanotherenergy.common.network.msg.MessageChunkPollution;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;

import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.world.ChunkDataEvent;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.event.world.ChunkWatchEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * Capability to store per-chunk pollution values.
 * 
 * @author MinecraftPlaye
 */
public class CapabilityPollution {
    
    /**
     * The {@link Capability} instance.
     */
    @CapabilityInject(IChunkPollutionHolder.class)
    public static final Capability<IChunkPollutionHolder> CHUNK_POLLUTION_CAPABILITY = null;
    
    /**
     * The default {@link EnumFacing} to use for this capability.
     */
    public static final EnumFacing DEFAULT_FACING = null;
    
    /**
     * The ID of this capability.
     */
    private static final ResourceLocation ID = new ResourceLocation(Constants.MOD_ID, "chunk_pollution");
    
    /**
     * The ID of this capability.
     */
    private static final String ID_STRING = ID.toString();
    
    /**
     * Registers this capability.
     */
    public static void register() {
        CapabilityManager.INSTANCE.register(IChunkPollutionHolder.class, new Capability.IStorage<IChunkPollutionHolder>() {
            
            @Override
            public NBTBase writeNBT(Capability<IChunkPollutionHolder> capability, IChunkPollutionHolder instance,
                    EnumFacing side) {
                return new NBTTagCompound();
            }
            
            @Override
            public void readNBT(Capability<IChunkPollutionHolder> capability, IChunkPollutionHolder instance,
                    EnumFacing side, NBTBase nbt) {
                // We don't need anything to do here, as we don't write data in there
            }
        }, ChunkPollutionHolder::new);
    }
    
    /**
     * Gets the {@link IChunkPollutionHolder} for the specified world.
     * 
     * @param world The world.
     * @return The {@link IChunkPollutionHolder} for the world.
     */
    public static IChunkPollutionHolder getChunkPollutionHolder(World world) {
        if(world != null && world.hasCapability(CHUNK_POLLUTION_CAPABILITY, DEFAULT_FACING))
            return world.getCapability(CHUNK_POLLUTION_CAPABILITY, DEFAULT_FACING);
        return null;
    }
    
    @SubscribeEvent
    public void attachCapabilitity(AttachCapabilitiesEvent<World> event) {
        final IChunkPollutionHolder chunkPollutionHolder = new ChunkPollutionHolder();
        event.addCapability(ID, new CapabilityProviderPollution(chunkPollutionHolder));
    }
    
    @SubscribeEvent
    public void chunkLoad(ChunkEvent.Load event) {
        final World world = event.getWorld();
        final ChunkPos pos = event.getChunk().getPos();
        
        final IChunkPollutionHolder chunkPollutionHolder = getChunkPollutionHolder(world);
        if(chunkPollutionHolder == null) return;
        
        // don't create a IChunkPollution twice for a chunk
        if(chunkPollutionHolder.getChunkPollution(pos) != null) return;
        
        final IChunkPollution chunkPollution = new ChunkPollution(world, pos);
        chunkPollutionHolder.setChunkPollution(pos, chunkPollution);
    }
    
    @SubscribeEvent
    public void onChunkUnload(ChunkEvent.Unload event) {
        final World world = event.getWorld();
        
        final IChunkPollutionHolder chunkPollutionHolder = getChunkPollutionHolder(world);
        if(chunkPollutionHolder == null) return;
        
        chunkPollutionHolder.removeChunkPollution(event.getChunk().getPos());
    }
    
    @SubscribeEvent
    public void onChunkDataLoad(ChunkDataEvent.Load event) {
        final World world = event.getWorld();
        final ChunkPos pos = event.getChunk().getPos();
        
        final IChunkPollutionHolder chunkPollutionHolder = getChunkPollutionHolder(world);
        if(chunkPollutionHolder == null) return;
        final ChunkPollution chunkPollution = new ChunkPollution(world, pos);
        
        final NBTTagCompound chunkData = event.getData();
        if(chunkData.hasKey(ID_STRING, NBT.TAG_DOUBLE)) {
            final NBTTagDouble pollutionTag = (NBTTagDouble) chunkData.getTag(ID_STRING);
            chunkPollution.deserializeNBT(pollutionTag);
        }
        chunkPollutionHolder.setChunkPollution(pos, chunkPollution);
    }
    
    @SubscribeEvent
    public void onChunkSave(ChunkDataEvent.Save event) {
        final World world = event.getWorld();
        
        final IChunkPollutionHolder chunkPollutionHolder = getChunkPollutionHolder(world);
        if(chunkPollutionHolder == null) return;
        
        final IChunkPollution chunkPollution = chunkPollutionHolder.getChunkPollution(event.getChunk().getPos());
        if(!(chunkPollution instanceof ChunkPollution)) return;
        event.getData().setTag(ID_STRING, ((ChunkPollution)chunkPollution).serializeNBT());
    }
    
    @SubscribeEvent
    public void onChunkWatch(ChunkWatchEvent.Watch event) {
        final EntityPlayerMP player = event.getPlayer();
        final World world = player.getEntityWorld();
        
        final IChunkPollutionHolder chunkPollutionHolder;
        if(world.hasCapability(CHUNK_POLLUTION_CAPABILITY, DEFAULT_FACING))
            chunkPollutionHolder = world.getCapability(CHUNK_POLLUTION_CAPABILITY, DEFAULT_FACING);
        else return;
        
        final IChunkPollution chunkPollution = chunkPollutionHolder.getChunkPollution(event.getChunk());
        NetworkHandler.INSTANCE.sendTo(new MessageChunkPollution(event.getChunk(), chunkPollution), player);
    }
}