/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.power.pollution;

import minecraftplaye.justanotherenergy.api.pollution.IChunkPollution;

import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;

import net.minecraftforge.common.util.INBTSerializable;

/**
 * Default implementation of {@link IChunkPollution}.
 * 
 * @author MinecraftPlaye
 */
public class ChunkPollution implements IChunkPollution, INBTSerializable<NBTTagDouble> {
    
    /**
     * The {@link World} containing the chunk.
     */
    private final World world;
    
    /**
     * The {@link ChunkPos} of the chunk.
     */
    private final ChunkPos pos;
    
    /**
     * The pollution of the chunk.
     */
    private double pollution;
    
    public ChunkPollution(final World world, final ChunkPos pos) {
        this.world = world;
        this.pos = pos;
        this.pollution = 0;
    }
    
    @Override
    public void polluteChunk(double pollution) {
        this.pollution += pollution;
    }
    
    @Override
    public void decreasePollution(double pollution) {
        if(this.pollution - pollution < 0)
            this.pollution = 0;
        else this.pollution -= pollution;
    }
    
    @Override
    public void setPollution(double pollution) {
        this.pollution = pollution >= 0 ? pollution : 0;
    }
    
    @Override
    public double getPollution() {
        return this.pollution >= 0 ? this.pollution : 0;
    }
    
    @Override
    public World getWorld() {
        return this.world;
    }
    
    @Override
    public ChunkPos getChunkPos() {
        return this.pos;
    }
    
    @Override
    public NBTTagDouble serializeNBT() {
        return new NBTTagDouble(this.getPollution());
    }
    
    @Override
    public void deserializeNBT(NBTTagDouble nbt) {
        this.pollution = nbt.getDouble();
        
        if(this.pollution < 0)
            this.pollution = 0;
    }
}