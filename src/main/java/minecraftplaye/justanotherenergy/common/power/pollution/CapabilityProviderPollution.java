/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.power.pollution;

import minecraftplaye.justanotherenergy.api.pollution.IChunkPollutionHolder;

import net.minecraft.util.EnumFacing;

import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;

public class CapabilityProviderPollution implements ICapabilityProvider {
    
    private IChunkPollutionHolder instance;
    
    public CapabilityProviderPollution(IChunkPollutionHolder instance) {
        this.instance = instance;
    }
    
    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
        return capability == CapabilityPollution.CHUNK_POLLUTION_CAPABILITY;
    }
    
    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
        if(capability == CapabilityPollution.CHUNK_POLLUTION_CAPABILITY)
            return CapabilityPollution.CHUNK_POLLUTION_CAPABILITY.cast(this.instance);
        return null;
    }
}