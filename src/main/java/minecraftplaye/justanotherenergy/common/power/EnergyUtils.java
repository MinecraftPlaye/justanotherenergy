/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.power;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import minecraftplaye.justanotherenergy.common.power.EnergyManager.EnergyTransfer;
import minecraftplaye.justanotherenergy.common.power.tesla.TeslaHelper;
import minecraftplaye.justanotherenergy.common.tileentities.TileEntityEnergy;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;

import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

public class EnergyUtils {
    
    public static int sendEnergy(@Nonnull TileEntity from, @Nonnull TileEntity sendTo, @Nullable EnumFacing orientation, @Nonnull int transferAmount) {
        if(from == null || sendTo == null || transferAmount <= 0)
            return 0;
        
        EnumFacing side = orientation != null ? orientation.getOpposite() : (EnumFacing) null;
        
        // Mod internal only
        if(from instanceof TileEntityEnergy && sendTo instanceof TileEntityEnergy) {
            TileEntityEnergy fromTile = (TileEntityEnergy) from;
            TileEntityEnergy sendToTile = (TileEntityEnergy) sendTo;
            if(sendToTile.getEnergyManager().canReceive() && fromTile.getEnergyManager().canExtract())
                return sendToTile.getEnergyManager().receiveEnergy(fromTile.getEnergyManager().extractEnergy(transferAmount, false), false);
        } else {
            // Forge Energy Support
            if(from.hasCapability(CapabilityEnergy.ENERGY, orientation)) {
                IEnergyStorage fromStorage = from.getCapability(CapabilityEnergy.ENERGY, side);
                if(sendTo.hasCapability(CapabilityEnergy.ENERGY, side)) {
                    IEnergyStorage sendToStorage = sendTo.getCapability(CapabilityEnergy.ENERGY, side);
                    if(fromStorage != null && fromStorage.canExtract() && fromStorage.getEnergyStored() > 0)
                        if(sendToStorage != null && sendToStorage.canReceive() && sendToStorage.getEnergyStored() < sendToStorage.getMaxEnergyStored())
                            return sendToStorage.receiveEnergy(fromStorage.extractEnergy(transferAmount, false), false);
                // Tesla Energy Support
                } else if(fromStorage != null && fromStorage.canExtract() && fromStorage.getEnergyStored() > 0 && TeslaHelper.isEnergyReceiver(sendTo, side))
                    return TeslaHelper.sendEnergy(sendTo, side, fromStorage.extractEnergy(transferAmount, false), false);
            }
        }
        
        return 0;
    }
    
    public static int sendEnergy(@Nonnull TileEntityEnergy from, @Nonnull TileEntity sendTo, @Nullable EnumFacing orientation, @Nonnull int transferAmount) {
        if(from == null || sendTo == null || transferAmount <= 0)
            return 0;
        
        EnumFacing side = orientation != null ? orientation.getOpposite() : (EnumFacing) null;
        
        // Mod internal only
        if(sendTo instanceof TileEntityEnergy) {
            TileEntityEnergy tile = (TileEntityEnergy) sendTo;
            if(tile.getEnergyManager().canReceive() && from.getEnergyManager().canExtract())
                return tile.getEnergyManager().receiveEnergy(from.getEnergyManager().extractEnergy(transferAmount, false), false);
        } else {
            // Forge Energy Support
            if(sendTo.hasCapability(CapabilityEnergy.ENERGY, side)) {
                IEnergyStorage storage = sendTo.getCapability(CapabilityEnergy.ENERGY, side);
                if(storage != null && storage.canReceive() && storage.getEnergyStored() < storage.getMaxEnergyStored() && from.getEnergyManager().canExtract())
                    return storage.receiveEnergy(from.getEnergyManager().extractEnergy(transferAmount, false), false);
            // Tesla Energy Support
            } else if(TeslaHelper.isEnergyReceiver(sendTo, side))
                return TeslaHelper.sendEnergy(sendTo, side, from.getEnergyManager().extractEnergy(transferAmount, false), false);
        }
        return 0;
    }
    
    public static boolean consumeEnergy(@Nonnull TileEntityEnergy consumer, @Nonnull int amount) {
       if(consumer == null || consumer.getEnergyManager().getEnergyStored() < amount)
           return false;
       
       if(consumer.getEnergyManager().getTransferMode() == EnergyTransfer.CONSUMER ||
    		   consumer.getEnergyManager().getTransferMode() == EnergyTransfer.BOTH) {
    	   if(consumer.getEnergyManager().canExtract()) {
    		   consumer.getEnergyManager().extractEnergy(amount, false);
    		   return true;
    	   }
       }
       return false;
    }
    
    /**
     * Has the tile entity the capability to hold energy?
     * This applies only for energy of the type Tesla or
     * Forge Units (FU).
     * 
     * @param tile The tile entity.
     * @param orientation The direction where the tile entity can either accept or output energy.
     * @return Has the tile entity the capability to hold energy?
     */
    public static boolean hasCapability(TileEntity tile, EnumFacing orientation) {
    	EnumFacing side = orientation != null ? orientation.getOpposite() : (EnumFacing) null;
    	
    	if(tile.hasCapability(CapabilityEnergy.ENERGY, side)) return true;
    	else if(TeslaHelper.isEnergyHolder(tile, side)) return true;
    	else if(TeslaHelper.isEnergyProducer(tile, side)) return true;
    	else if(TeslaHelper.isEnergyReceiver(tile, side)) return true;
    	
    	return false;
    }
}