/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.power.forge;

import minecraftplaye.justanotherenergy.common.power.EnergyManager;
import minecraftplaye.justanotherenergy.common.power.EnergyManager.EnergyTransfer;
import net.minecraftforge.energy.IEnergyStorage;

public class ForgeEnergyWrapper implements IEnergyStorage {
    
    private final EnergyManager manager;
    private final EnergyTransfer transfer;
    
    public ForgeEnergyWrapper(EnergyManager manager) {
        this.manager = manager;
        this.transfer = manager.getTransferMode();
    }
    
    @Override
    public int receiveEnergy(int maxReceive, boolean simulate) {
        return this.canReceive() ? this.manager.receiveEnergy(maxReceive, simulate) : 0;
    }
    
    @Override
    public int extractEnergy(int maxExtract, boolean simulate) {
        return this.canExtract() ? this.manager.extractEnergy(maxExtract, simulate) : 0;
    }
    
    @Override
    public int getEnergyStored() {
        return this.manager.getEnergyStored();
    }
    
    @Override
    public int getMaxEnergyStored() {
        return this.manager.getMaxEnergyStored();
    }
    
    @Override
    public boolean canExtract() {
        return this.transfer.canExtract() && this.manager.canExtract();
    }
    
    @Override
    public boolean canReceive() {
        return this.transfer.canReceive() && this.manager.canReceive();
    }
}