/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy.common.power;

import javax.annotation.Nullable;

import minecraftplaye.justanotherenergy.common.power.forge.ForgeEnergyWrapper;
import minecraftplaye.justanotherenergy.common.power.tesla.TeslaConsumerWrapper;
import minecraftplaye.justanotherenergy.common.power.tesla.TeslaHelper;
import minecraftplaye.justanotherenergy.common.power.tesla.TeslaHolderWrapper;
import minecraftplaye.justanotherenergy.common.power.tesla.TeslaProducerWrapper;

import net.darkhax.tesla.Tesla;
import net.darkhax.tesla.api.ITeslaConsumer;
import net.darkhax.tesla.api.ITeslaHolder;
import net.darkhax.tesla.api.ITeslaProducer;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;

import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.energy.IEnergyStorage;

/**
 * A basic implementation of the Forge Energy API defined
 * in {@link IEnergyStorage} with additional support for the 
 * {@link Tesla} API. This implementation allows additionally
 * the support to save data through NBT using the {@link INBTSerializable}
 * interface.
 * <p>
 * All entities (in general) of any kind are required to hold an instance to this class
 * to be able to deal with energy related tasks.
 * <p>
 * <i>Note: Additional behavior was added to allow native Tesla support as
 * Forge doesn't come with support to differentiate between consumer and producer
 * out of the box which is a required behavior from the Tesla API. Furthermore should
 * it be noted that the maximum capacity of this implementation is the maximum size
 * of an integer in Java. Thus all smaller or larger values will be converted to match
 * the requirement. This <b>might cause energy loss</b> when connected to an Tesla based
 * energy network! Be aware of this behavior when using this implementation.</i>
 * 
 * @author MinecraftPlaye
 */
public class EnergyManager extends EnergyStorage implements INBTSerializable<NBTTagCompound> {
    
	// value to determine if the values in this manager have changed during runtime
	private boolean markDirty = false;
    private EnergyTransfer transfer = EnergyTransfer.BOTH;
    
    /**
     * Creates a new instance of this manager to allow
     * the use of energy to whatever entity might has implemented 
     * this.
     * 
     * @param capacity the maximum amount of energy which can be stored
     * @param maxTransfer the maximum amount of energy which can be extracted or
     *                    received at a given point in time
     */
    public EnergyManager(int capacity, int maxTransfer) {
        super(capacity, maxTransfer);
    }
    
    /**
     * Creates a new instance of this manager to allow
     * the use of energy to whatever entity might has implemented 
     * this.
     * 
     * @param capacity the maximum amount of energy which can be stored
     * @param maxReceive the maximum amount of energy which can be received at at a given point in time
     * @param maxExtract the maximum amount of energy which can be extracted at at a given point in time
     */
    public EnergyManager(int capacity, int maxReceive, int maxExtract) {
        super(capacity, maxReceive, maxExtract);
    }
    
    @Override
    public NBTTagCompound serializeNBT() {
        final NBTTagCompound nbtTagCompound = new NBTTagCompound();
        
        nbtTagCompound.setInteger("capacity", this.capacity);
        nbtTagCompound.setInteger("energy", this.energy);
        nbtTagCompound.setInteger("extract", this.maxExtract);
        nbtTagCompound.setInteger("receive", this.maxReceive);
        
        return nbtTagCompound;
    }
    
    @Override
    public void deserializeNBT(NBTTagCompound nbt) {
        if(nbt.hasKey("capacity"))
        	this.capacity = nbt.getInteger("capacity");
        
        if(nbt.hasKey("energy")) {
            final int tempEnergy = nbt.getInteger("energy");
            
            // prevent modifying stored energy
            if(tempEnergy >= this.capacity)
                this.energy = this.capacity;
            else if(tempEnergy < this.capacity)
                this.energy = tempEnergy;
            else this.energy = 0;
        }
        
        if(nbt.hasKey("extract"))
        	this.maxExtract = nbt.getInteger("extract");
        if(nbt.hasKey("receive"))
        	this.maxReceive = nbt.getInteger("receive");
    }
    
    @Override
    public boolean canExtract() {
        return super.canExtract() && this.getEnergyStored() > 0;
    }
    
    @Override
    public boolean canReceive() {
        return super.canReceive() && this.getEnergyStored() < this.getMaxEnergyStored();
    }
    
    @Override
    public int receiveEnergy(int maxReceive, boolean simulate) {
    	int result = super.receiveEnergy(maxReceive, simulate);
    	
    	if(result > 0)
    		this.markForChange();
    	
    	return result;
    }
    
    @Override
    public int extractEnergy(int maxExtract, boolean simulate) {
    	int result = super.extractEnergy(maxExtract, simulate);
    	
    	if(result > 0)
    		this.markForChange();
    	
    	return result;
    }
    
    /**
     * Sets the stored energy of this manager.
     * This method is used <b>only</b> by the UI
     * system to update its values to display.
     * 
     * @param power the new capacity of the manager
     * 
     * @deprecated May be removed in the future. The
     * new introduced UI system doesn't require this
     * method anymore.
     */
    @Deprecated
    public void setEnergyStored(int power) {
        if(power >= this.capacity)
            this.energy = this.capacity;
        else if(power < this.capacity)
            this.energy = power;
        
        this.markForChange();
    }
    
    /**
     * Returns the stored capacity to display.
     * This is usually the actual capacity minus 1.
     * It can be used to show the player the capacity
     * while also having a buffer. <p>This buffer is currently
     * used to simulate energy overflow.
     * 
     * @return the stored capacity to display
     */
    public int getDisplayMaxStored() {
        return this.capacity - 1;
    }
    
    /**
     * Sets the transfer mode for this manager, meaning
     * whether energy can be produced or consumed.
     * 
     * @param mode the mode to set the manager to
     * @see EnergyTransfer
     */
    public void setTransferMode(EnergyTransfer mode) {
        this.transfer = mode;
    }
    
    /**
     * Returns whether this manager can consume or
     * produce energy.
     * 
     * @return the transfer mode
     * @see EnergyTransfer
     */
    public EnergyTransfer getTransferMode() {
        return this.transfer;
    }
    
    /**
     * Marks that this manager has been changed.
     */
    private void markForChange() {
    	this.markDirty = true;
    }
    
    /**
     * Resets the value which determines if the energy
     * in this manager has changed. It is highly recommended
     * to call this method after {@link EnergyManager#hasChanged()}.
     * Without calling it {@link EnergyManager#hasChanged()} might
     * always return true!
     */
    public void resetHasChanged() {
        this.markDirty = false;
    }
    
    /**
     * Call this method to trigger events or
     * save data on the client side when the
     * energy in the manager has changed.
     * 
     * @return if the manager has changed.
     */
    public boolean hasChanged() {
    	return this.markDirty;
    }
    
    /**
     * Returns the maximum amount of energy which
     * can be transfered at the same time towards this block
     * or from this block to another.
     * <b>If the amount of energy which can be received or extracted
     * at the same time are different, the result will be -1!</b>
     * 
     * @return the maximum transfer rate this manager has
     */
    public int getMaxTransferRate() {
        return this.maxReceive == this.maxExtract ? this.maxReceive : -1;
    }
    
    /**
     * Retrieves the handler for the capability requested on the specific side. 
     * The return value CAN be null if the object does not support the capability. 
     * The return value CAN be the same for multiple faces.
     * 
     * @param capability the capability to request
     * @param facing the side to request the capability on
     * @return the handler for the requested capability or null
     */
    @Nullable
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityEnergy.ENERGY) {
            IEnergyStorage energyStorage = new ForgeEnergyWrapper(this);
            return CapabilityEnergy.ENERGY.cast(energyStorage);
        } else if(TeslaHelper.isLoaded()) {
            Capability<ITeslaHolder> teslaHolder = TeslaHelper.TESLA_HOLDER;
            Capability<ITeslaProducer> teslaProducer = TeslaHelper.TESLA_PRODUCER;
            Capability<ITeslaConsumer> teslaConsumer = TeslaHelper.TESLA_CONSUMER;
            
            if(capability == teslaConsumer && this.getTransferMode().canReceive())
                return teslaConsumer.cast(new TeslaConsumerWrapper(this));
            if(capability == teslaHolder)
                return teslaHolder.cast(new TeslaHolderWrapper(this));
            if(capability == teslaProducer && this.getTransferMode().canExtract())
                return teslaProducer.cast(new TeslaProducerWrapper(this));
            
            return null;
        } else
            return null;
    }
    
    /**
     * Determines if this object has support for the capability in question on the specific 
     * side. The return value of this <b>might<b> change during runtime if this object gains 
     * or looses support for a capability.
     * <p>
     * Example: A Pipe getting a cover placed on one side causing it loose the Inventory 
     * attachment function for that side. This is a light weight version of 
     * getCapability, intended for metadata uses.
     * 
     * @param capability the capability to check support for
     * @param facing the side of the capability 
     * @return true if the object has support for the capability on the specific side
     */
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityEnergy.ENERGY ||
               (capability == TeslaHelper.TESLA_CONSUMER && this.getTransferMode().canReceive()) ||
               capability == TeslaHelper.TESLA_HOLDER ||
               (capability == TeslaHelper.TESLA_PRODUCER && this.getTransferMode().canExtract());
    }
    
    /**
     * An enumeration to differentiate between
     * different types of energy users. This
     * enumeration is used to implement a similar
     * behavior as the {@link Tesla} API is using.
     * It has to be in place of here for full support
     * of the {@link Tesla} API as of the Forge Energy
     * API does not come with support for this behavior.
     * TODO: include link to API
     * 
     * @author MinecraftPlaye
     */
    public enum EnergyTransfer {
    	/**
    	 * The {@link EnergyManager} is allowed to produce energy.
    	 * Thus energy can only be extracted.
    	 */
        PRODUCER, 
        /**
         * The {@link EnergyManager} is allowed to consume energy.
         * Thus it can receive energy only, but nevertheless consume it.
         */
        CONSUMER, 
        /**
         * The {@link EnergyManager} is allowed to produce and consume energy.
         * <i>This is not recommended and may cause weird behavior when used
         * with cables or other energy users.</i>
         */
        BOTH, 
        /**
         * The {@link EnergyManager} is not allowed to deal with energy at all.
         * This value might be used for dynamic energy users which at some point
         * allow the consumption or production of energy while at another point
         * don't want to allow this behavior anymore. 
         */
        NONE;
        
    	/**
    	 * Returns true when energy can be extracted.
    	 * This is only the case when the {@link EnergyManager}
    	 * is of type producer or of both types.
    	 * The type {@link EnergyTransfer#NONE} means
    	 * no energy can be extracted.
    	 * 
    	 * @return Returns true when energy can be extracted.
    	 */
        public boolean canExtract() {
            switch(this) {
                case PRODUCER:
                case BOTH:
                    return true;
                case NONE:
                case CONSUMER:
                default:
                    return false;
            }
        }
        
    	/**
    	 * Returns true when energy can be received.
    	 * This is only the case when the {@link EnergyManager}
    	 * is of type consumer or of both types.
    	 * The type {@link EnergyTransfer#NONE} means
    	 * no energy can be received.
    	 * 
    	 * @return Returns true when energy can be received.
    	 */
        public boolean canReceive() {
            switch(this) {
                case CONSUMER:
                case BOTH:
                    return true;
                case PRODUCER:
                case NONE:
                default:
                    return false;
            }
        }
    }
}