/*
 * Copyright (c) 2017 MinecraftPlaye
 * All rights reserved.
 */

package minecraftplaye.justanotherenergy;

import minecraftplaye.justanotherenergy.client.gui.UIEventHandler;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.common.MinecraftForge;

public class ClientProxy extends ServerProxy {
	
    @Override
    public void registerEventHooks() {
        super.registerEventHooks();
        
        MinecraftForge.EVENT_BUS.register(new UIEventHandler());
    }
    
	@Override
	public String localize(String unlocalized, Object... args) {
		return I18n.format(unlocalized, args);
	}
}